#pragma once
typedef double DataType;

/* ListElement interface */
typedef struct ListElement_ ListElement;

ListElement *ListElementGetSuccessor(ListElement *element);
DataType ListElementGetValue(ListElement *element);

/* List interface */
typedef ListElement List;

List *ListCreate();
void ListDelete(List *list);

void ListClear(List *list);
int ListIsEmpty(List *list);

ListElement *ListGetFirstElement(List *list);

ListElement * ListGetLastElement(List *list);
ListElement * ListFindElement(List *list, DataType x);

void ListInsertAfterElement(ListElement *element, DataType x);
void ListRemoveAfterElement(ListElement *element);
void ListPrepend(List *list, DataType x);
void ListAppend(List *list, DataType x);