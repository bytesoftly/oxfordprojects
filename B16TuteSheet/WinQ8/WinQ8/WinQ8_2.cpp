// WinQ7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "list.h"

void print(List *list);
void merge_test();
void merge(List *merged_list, List *list1, List *list2);

int main()
{
	List* list;
	ListElement *element;

	list = ListCreate();

	ListAppend(list, 1);
	ListAppend(list, 2);
	ListAppend(list, 3);
	ListAppend(list, 4);
	print(list);

	element = ListFindElement(list, 3);
	ListInsertAfterElement(element, 10);
	print(list);

	ListRemoveAfterElement(ListFindElement(list, 2));
	print(list);

	ListClear(list);
	print(list);

	ListAppend(list, 3);
	print(list);

	ListDelete(list);

	/* do merge test */
	printf("Merge test:\n");
	merge_test();

	return 0;
}

/*
	Function tests the merge function written
*/
void merge_test()
{
	List *list1, *list2, *list3;

	list1 = ListCreate();
	list2 = ListCreate();
	list3 = ListCreate();

	ListAppend(list1, 1);
	ListAppend(list1, 3);
	ListAppend(list1, 4);
	ListAppend(list1, 7);

	ListAppend(list2, 2);
	ListAppend(list2, 5);
	ListAppend(list2, 6);

	merge(list3, list1, list2);

	print(list1);
	print(list2);
	print(list3);

	ListDelete(list3);
	ListDelete(list2);
	ListDelete(list1);
}

/*
	Function prints all contents of a list out
*/
void print(List *list)
{
	ListElement *element;
	
	/* set the first element */
	element = ListGetFirstElement(list);
	
	/* go through all list elements */
	while (element)
	{
		printf("%g\n", ListElementGetValue(element));
		element = ListElementGetSuccessor(element);
	}
}

/*
	Function merges list1 and list2 into merged_list
*/
void merge(List *merged_list, List *list1, List *list2)
{
	ListElement *l1_current;
	ListElement *l2_current;
	ListElement *l1_next;
	ListElement *l2_next;
	bool l1_up;
	bool l2_up;
	bool l1_bigger;

	/* get initial values */
	l1_current = ListGetFirstElement(list1);
	l2_current = ListGetFirstElement(list2);
	l1_next = ListElementGetSuccessor(l1_current);
	l2_next = ListElementGetSuccessor(l2_current);

	while (l1_current && l2_current)
	{
		/* check to match the element order in the new list */
		if (l1_next)
			l1_up = ListElementGetValue(l1_next) > ListElementGetValue(l1_current);
		if (l2_next)
			l2_up = ListElementGetValue(l2_next) > ListElementGetValue(l2_current);

		l1_bigger = ListElementGetValue(l1_current) > ListElementGetValue(l2_current);

		/* if both are ascending or descending at this point in the list then match it */
		if (l1_up == l2_up)
		{
			if (l1_up)
			{
				/* both ascending so append smaller then bigger */
				ListAppend(merged_list, ListElementGetValue(l1_bigger ? l2_current : l1_current));
				ListAppend(merged_list, ListElementGetValue(l1_bigger ? l1_current : l2_current));
			}
			else
			{
				/* both descending so append bigger then smaller */
				ListAppend(merged_list, ListElementGetValue(l1_bigger ? l1_current : l2_current));
				ListAppend(merged_list, ListElementGetValue(l1_bigger ? l2_current : l1_current));
			}
		}

		l1_current = l1_next;
		l2_current = l2_next;

		if (l1_next)
			l1_next = ListElementGetSuccessor(l1_next);
		if (l2_next)
			l2_next = ListElementGetSuccessor(l2_next);
	}
}