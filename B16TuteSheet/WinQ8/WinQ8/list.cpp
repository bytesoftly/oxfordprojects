#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* ???????????????????????????????????????????????????????????????? */
/* ListElement implementation */
/* ???????????????????????????????????????????????????????????????? */

typedef struct ListElement_
{
	struct ListElement_ *next;
	DataType data;
} ListElement;

ListElement * ListElementGetSuccessor(ListElement *element)
{
	if (element) 
	{
		return element->next;
	}
	else 
	{
		return NULL;
	}
}

DataType ListElementGetValue(ListElement *element)
{
	assert(element);
	return element->data;
}

/* ???????????????????????????????????????????????????????????????? */
/* List implementation */
/* ???????????????????????????????????????????????????????????????? */

List * ListCreate()
{
	List* list = (List*)malloc(sizeof(List));
	if (list)
	{
		list->next = NULL;
	}
	return list;
}

void ListDelete(List *list)
{
	assert(list);
	ListClear(list);
	free(list);
}

void ListClear(List *list)
{
	assert(list);
	while (!ListIsEmpty(list)) {
		ListRemoveAfterElement(list);
	}
}

int ListIsEmpty(List *list)
{
	return (list->next == NULL);
}

ListElement * ListGetFirstElement(List *list)
{
	assert(list);
	return list->next;
}

ListElement * ListGetLastElement(List *list)
{
	assert(list);
	ListElement* last = list;
	while (last->next) {
		last = last->next;
	}
	return last;
}

/*
	ListFindElement - Find one list element with the given data value
	param 'list' - pointer to list object to search through
	param 'x' - data to search for
	return - pointer to the list element or null if not found

	The function iterates through the linked list of elements until a
	data match is found.

	Errors: If no match is found null is returned, if the list pointer
	given is null.
*/
ListElement * ListFindElement(List *list, DataType x)
{
	assert(list);
	ListElement* current = ListGetFirstElement(list);
	while (current && current->data != x) {
		current = current->next;
	}
	return current;
}

/*
	ListInsertAfterElement - Inserts a new list element after the one 
	given
	return - void
	param 'element' - a pointer to the element to insert after
	param 'x' - contents of new list element

	The function allocates memory for a new element then updates
	pointers to correctly insert the element after that given and 
	before the next value (updates two links).

	Errors: Throws an error if heap allocation was unsuccessful or if
	the element argument given is a null pointer.
*/
void ListInsertAfterElement(ListElement *element, DataType x)
{
	assert(element);
	ListElement *newElement = (ListElement*)malloc(sizeof(ListElement));
	assert(newElement);
	newElement->next = element->next;
	newElement->data = x;
	element->next = newElement;
}

void ListRemoveAfterElement(ListElement *element)
{
	assert(element);
	ListElement *elementToRemove = element->next;
	if (elementToRemove) {
		element->next = elementToRemove->next;
		free(elementToRemove);
	}
}

void ListAppend(List *list, DataType x)
{
	assert(list);
	ListInsertAfterElement(ListGetLastElement(list), x);
}