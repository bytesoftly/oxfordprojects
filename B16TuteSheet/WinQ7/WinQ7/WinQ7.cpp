// WinQ7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "list.h"

int main()
{
	List* list;
	ListElement *element;
	list = ListCreate();
	ListAppend(list, 1);
	ListAppend(list, 2);
	ListAppend(list, 3);
	ListAppend(list, 4);
	ListDelete(list);
	return 0;
}

