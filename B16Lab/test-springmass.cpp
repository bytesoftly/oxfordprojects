/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

int main(int argc, char** argv)
{
	const double mass = 0.05 ;
	const double radius = 0.02 ;
	const double naturalLength = 0.95 ;
	const double stiffness = 1;
	const double damping = 0.01;
    
    /* instantiate two masses */
	Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
	Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;

	m1.setForce(Vector2(0, 0));
	m2.setForce(Vector2(0, 0));
    
    /* instantiate spring and pass in address of m1 and m2 */
	Spring spring(&m1, &m2, naturalLength, stiffness, damping);

	/* instantiate simulation and pass in address of m1 and m2 */
	SpringMass springmass(&spring, &m1, &m2, 0);

	const double dt = 1.0/30 ;
	for (int i = 0 ; i < 200 ; ++i) 
	{
		springmass.step(dt) ;
		springmass.display() ;
	}

	return 0 ;
}
