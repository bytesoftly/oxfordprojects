/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi/James Clemoes
 **/

#include "springmass.h"

#include <iostream>

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
	double energy = 0;

	/* energy of mass = KE + GPE */
	energy = mass*(0.5*velocity.norm2() + (ymax - position.y));

	return energy;
}

void Mass::step(double dt)
{
	double new_x;
	double new_y;
	double new_v_x;
	double new_v_y;

	/* assuming constant acceleration and velocity in a small time period, using s = ut + 0.5*a*t^2 */
	new_x = position.x + velocity.x * dt + 0.5 * force.x * dt * dt / mass;
	new_y = position.y + velocity.y * dt + 0.5 * force.y * dt * dt / mass;

	/* calculate the new velocities, using v = u+a*t */
	new_v_x = velocity.x + force.x * dt / mass;
	new_v_y = velocity.y + force.y * dt / mass;

	/* check if we've hit either of the side walls */
	if (new_x - radius < xmin || new_x + radius > xmax)
		velocity.x = 0;
	else
	{
		position.x = new_x;
		velocity.x = new_v_x;
	}
		
	/* check if we've hit the top or the bottom */
	if (new_y + radius > ymax || new_y - radius < ymin)
		velocity.y = 0;
	else
	{
		position.y = new_y;
		velocity.y = new_v_y;
	}
		
}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping)
: mass1(mass1), mass2(mass2),
naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1;
}

Mass * Spring::getMass2() const
{
  return mass2;
}

Vector2 Spring::getForce() const
{
	Vector2 F;
	Vector2 spring_vector;
	Vector2 spring_velocity_vector;
	Vector2 u12;
	double l;
	double v;

	/* get the norm of the spring ends position and velocity vector */
	spring_vector = mass2->getPosition() - mass1->getPosition();
	l = spring_vector.norm();

	/* calculate a velocity vector */
	spring_velocity_vector = mass2->getVelocity() - mass1->getVelocity();
	v = spring_velocity_vector.norm();

	/* calculate the force in the spring = spring force - spring damping */
	u12 = (1 / l)*spring_vector;
	F = stiffness*(l - naturalLength)*u12 -damping*dot(spring_velocity_vector, u12)*u12;
	
	return F;
}

double Spring::getLength() const
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm() ;
}
/*
double Spring::getVelocity() const
{
	Vector2 u = mass2->getVelocity() - mass1->getVelocity();
	return u.norm();
}
*/
double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(Spring *spring, Mass *mass1, Mass *mass2, double gravity)
: gravity(gravity), spring(spring), mass1(mass1), mass2(mass2)
{ }



void SpringMass::display()
{
	Vector2 pos1 = mass1->getPosition();
	Vector2 pos2 = mass2->getPosition();

	/* print a column in format x1, y1, x2, y2 */
	/* todo: this should be a for loop for multiple springs and masses */
	std::cout << pos1.x << " " << pos1.y << " " << pos2.x << " " << pos2.y << std::endl;
}

double SpringMass::getEnergy() const
{
  double energy = 0;

  /* this should be a for loop */
  energy = mass1->getEnergy(gravity) + mass2->getEnergy(gravity) + spring->getEnergy();

  return energy;
}

void SpringMass::step(double dt)
{
	Vector2 g(0,-gravity);
	Vector2 spring_force;

	/* add gravity force to each mass */
	/* todo: put this in a for loop */
	mass1->setForce(g);
	mass2->setForce(g);

	/* add the spring forces to each mass */
	/* todo: put this in a for loop */
	spring_force = spring->getForce();
	mass1->addForce(spring_force);
	mass2->addForce(-1*spring_force);

	/* step all the mass models simultaneously */
	/* todo: put this in a for loop */
	mass1->step(dt);
	mass2->step(dt);
}


