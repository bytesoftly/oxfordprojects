/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi/James Clemoes
 **/

#include "ball.h"

#include <iostream>

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

Ball::Ball(double x, double y, double vx, double vy) : r(0.1), g(9.8), m(1),
	xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

void Ball::step(double dt)
{
	double xp;
	double yp;
	
	/* calculate the expected new positions from the physics */
	xp = x + vx * dt;
	yp = y + vy * dt - 0.5 * g * dt * dt;

	/* constrain the ball to the defined box in the x direction */
	if (xmin + r <= xp && xp <= xmax - r) 
	{
		x = xp ;
	} 
	else 
	{
		/* the ball has bounced so assuming perfectly elastic reverse the vertical velocity */
		vx = -vx ;
	}                                             

	/* same logic in the other direction */
	if (ymin + r <= yp && yp <= ymax - r) 
	{
		y = yp ;
		vy = vy - g * dt ;
	} 
	else 
	{
		vy = -vy ;
	}

}

void Ball::display()
{
	/* print out the x and y coordinates in the stdout of the cin */
	std::cout<<x<<" "<<y<<std::endl ;
}
