% load the raw output of ball.exe from the command line
cd '../Debug'
load 'sim.txt';
cd '../matlab'

% plot the set of x,y coordinates
plot(sim(:,1), sim(:,2));
hold all;
plot(sim(:,3), sim(:,4));
title('Mass motion');
xlabel('x');
ylabel('y');
