clear;
close all;

% load the raw output of ball.exe from the command line
cd '../Debug'
load 'sim.txt';
cd '../matlab'

for idx = 1:length(sim)
    % plot the set of x,y coordinates
    title('Mass motion');
    xlabel('x');
    ylabel('y');
    plot(sim(idx,1), sim(idx,2), 'o');
    hold all
    plot(sim(idx,3), sim(idx,4), 'o');
    axis([-1 1 -1 1]);
    drawnow;
    pause(0.01);
    hold off;
end
