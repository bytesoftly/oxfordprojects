% load the raw output of ball.exe from the command line
cd '../Debug'
load 'ball.txt';
cd '../matlab'

% plot the set of x,y coordinates
plot(ball(:,1), ball(:,2));
title('Ball motion');
xlabel('x');
ylabel('y');
