% James Clemoes 6/1/2016

% create some (integer) values of m and n
m = 1:100;
n = 1:100;

% parameters for fs_orthog
T = 1; % period (sec)
nint = 1000; % number of samples

% preallocate for speed
cos_cos_img = zeros(length(m), length(n)); % m rows n cols
sin_sin_img = zeros(length(m), length(n));
sin_cos_img = zeros(length(m), length(n));

% create the images
for n_current = n
    for m_current = m
        % cos cos case
        [result, status] = fs_orthog(T, nint, m_current, n_current, 'cc');
        if status
            cos_cos_img(m_current, n_current) = result;
        end
        
        % sin sin case
        [result, status] = fs_orthog(T, nint, m_current, n_current, 'ss');
        if status
            sin_sin_img(m_current, n_current) = result;
        end
        
        % sin cos case
        [result, status] = fs_orthog(T, nint, m_current, n_current, 'sc');
        if status
            sin_cos_img(m_current, n_current) = result;
        end
    end
end

% show the images
imshow(cos_cos_img);
figure;
imshow(sin_sin_img);
figure;
imshow(sin_cos_img);

% write the images
imwrite(cos_cos_img, '../FigImages/orthog_cos_cos.png');
imwrite(sin_sin_img, '../FigImages/orthog_sin_sin.png');
imwrite(sin_cos_img, '../FigImages/orthog_sin_cos.png');
