% Compute Fourier coeffs and series upto nterms for a triangle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fs_triangle.m DWM 6/9/10
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fs_triangle_tails(T,nint,nterms)

% validate T parameter (impose range of -0.5 <= T <= 0.5)
if T > 0.5
  T = 0.5;
elseif T < -0.5
  T = -0.5;
end

% derive omega
w = 2*pi;

% create a range of x values to create the triangle with
x = linspace(-T, T, 2*T*nint);

% calculate the triangle
f = fs_periodictriangle(T*2,x);

% create a row of zeros
y = zeros(1, nint);

% calculate the tail width in samples (round up)
t_width = (nint - length(x))*0.5;

% super-impose the triangle onto the zeros (take care not to make an off by
% one error
y(t_width+1:end-t_width) = f;

% create new x values (we want exactly nint)
x = linspace(-0.5, 0.5, nint);
  
% Compute coefficients 
A0 = fs_Acoeff(1,0,y,x);      % A0 done separately
for m=1:1:nterms
    A(m) = fs_Acoeff(1,m,y,x);  % The rest in a loop
    B(m) = fs_Bcoeff(1,m,y,x);  %
end
  
% For fun, build the series approximation to nterms
fseries = (A0/2)*ones(size(x));
for m=1:1:nterms
    fseries = fseries + A(m)*cos(m*w*x) + B(m)*sin(m*w*x);
end
  
% Plot original triangular function
hold off;
set(gca, 'FontSize', 18);
plot(x, y, '-', 'LineWidth',3, 'Color', [0.8 0 0]);
xlabel('x'); ylabel('Triangle with Tails Function'); hold on;
  
% Save diagram as colour postscript
print('-dpng', '../FigImages/fs_triangle_tails.png');
  
% Plot FS approximation to nterms
hold off;
figure;
set(gca, 'FontSize', 18);
plot(x, fseries, '-', 'LineWidth',3, 'Color', [0.0 0.0 0.8]);
xlabel('x'); 
ylabel(sprintf('Fourier series: %d terms',nterms));
hold on;
  
% Save diagram as colour postscript
% Filename will include the number of terms. 
print('-dpng', sprintf('../FigImages/fs_triangle_tails%d.png',nterms));

