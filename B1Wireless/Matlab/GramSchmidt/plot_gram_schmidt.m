%PLOT_GRAM_SCHMIDT Demonstrates the Gram Schmidt implementation over a
%specific x range

num_functions = 6;

% calculate over the range leading to 99.9% of the regular inner product
% (due to our modified inner product definition).
lim = -log(0.001);
x = linspace(0, lim); % create 100 terms

% calculate the values by Gram Schmidt 
[g_n, g] = gram_schmidt(num_functions, x);

% plot a graph of the orthonormal set
for n = 1:num_functions
    plot(x, g_n(n, :), 'DisplayName', sprintf('order %d', n-1));
    hold on;
end
legend(gca, 'show', 'Location', 'northwest');
title('Orthonormal set from a monomial base set (via Gram Schmidt)');
xlabel('x');
ylabel('||g_{n}(x)||');
grid on;
grid minor;

% save the graph
print('-dpng', sprintf('../../FigImages/gram_schmidt_%d_functions.png', num_functions));

% plot a graph of the orthogonal (non-normalised) set
% figure;
% for n = 1:num_functions
%     plot(x, g(n, :));
%     hold on;
% end
% title('Orthogonal set from a monomial base set (via Gram Schmidt)');
% xlabel('x');
% ylabel('g_{n}(x)');
% grid on;
% grid minor;
% 
% % save the graph
% print('-dpng', sprintf('../../FigImages/gram_schmidt_%d_functions_unnormalised.png', num_functions));
