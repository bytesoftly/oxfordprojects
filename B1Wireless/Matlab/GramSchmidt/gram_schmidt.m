function [ g_n, g ] = gram_schmidt(num_functions, x)
%GRAM_SCHMIDT Generates numerical values of an orthogonal set via the Gram
%Schmidt method. Returns both orthogonal and orthonormal sets generated.
% James Clemoes 3/1/2016

% preallocate space for output
n_terms = length(x);
v = zeros(num_functions, n_terms);      % linearly independent functions
g = zeros(num_functions, n_terms);      % orthogonal functions
g_n = zeros(num_functions, n_terms);    % orthonormal functions

% use Gram-Schmidt to generate the required number of orthonormal functions
for n = 1:num_functions    
    % calculate the arbitrary base function values (using monomials here)
    v(n, :) = calc_monomial(x, n-1);
    
    e = zeros(1, n-1); % create a matrix of the e values for printing out later
    
    % allow for special case at the start
    if (n == 1)
        % make orthogonal function (nothing previous to be orthogonal to
        % yet so just duplicate the first base function.
        g(1,:) = v(1,:);
    else
        % calculate the coefficient orthogonal function and build the
        % orthogonal function
        g(n, :) = v(n, :);
        for m = 1:n - 1
            e(m) = calc_gs_coeff(v(n, :), g(m, :), x);
            g(n, :) = g(n, :) - e(m)*g(m, :);
        end

        % check the function is orthogonal to all the previous ones
        orth = true;
        for m = 1:n - 1
            inner_prod = exp_inner(g(n, :), g(m, :), x);
            if ~is_very_small(inner_prod)
                orth = false;
                disp(['Function ', num2str(n), ' is not orthogonal with function ', num2str(m), ' as inner product = ', num2str(inner_prod)]);
                break;
            end
        end
        if orth
            disp(['Function ', num2str(n), ' is orthogonal to all previous functions']);
        end
    end
    
    % normalise the orthogonal function to make it orthonormal (before
    % normalising we actually have the Legendre polynomials when using the
    % regular form of the inner product without the exponential)
    g_n(n, :) = g(n, :)/calc_norm(g(n, :), x);
    
    % do some reporting
    disp(['Function ', num2str(n), ' has norm=', num2str(calc_norm(g_n(n, :), x)), ', e=', mat2str(e)]);
end

end

function [ factor ] = calc_norm(g, x)
%CALC_NORM_FACTOR Calculates the normalising factor for a vector (L2 norm)
factor = exp_inner(g, g, x)^0.5;

end

function [ e ] = calc_gs_coeff(v, g, x)
%CALC_GS_COEFFS Calculates the Gram Schmidt coefficient used to create an
%orthogonal set (derived from orthogonality of all g functions)

% calculate the coefficients
e = exp_inner(v, g, x)/exp_inner(g, g, x);

end

function [ y ] = calc_monomial(x_vals, order)
%CALC_MONOMIAL Calculates the values of the output of a monomial, e.g. x,
%x^2, x^n where n is the power

y = power(x_vals, order);
if isinf(y)
    error('Evaluation of the monomial failed due to numeric overflow');
end

end

function [ inner ] = exp_inner(g_n, g_m, x)
%EXP_INNER Calculates the inner product with an exponential term present
%given the range of x values for the exponential term

% check the number of elements given above match (could indicate g_n and
% g_m were not calculated with the given x which would make the result
% invalid)
l1 = length(g_n);
assert((l1 == length(g_m)) & (l1 == length(x)), 'Length of input vectors do not match');

% calculate the inner product with an exponential term included. Either
% method of integration both lead to a similar result
%inner = sum(g_n.*g_m.*exp(-x));     % cheap inner product with exponential term
inner = trapz(x, g_n.*g_m.*exp(-x));   % more accurate integrator (using trapezium rule)
%inner = trapz(x, g_n.*g_m);
%inner = dot(g_n,g_m);          % regular inner product

end

function [ result ] = is_very_small(number)
%IS_VERY_SMALL Checks if a number is zero to the precision of 10^-10 - i.e.
%a number that may have been zero if not for floating point precision.
result = number < 10^-10;

end