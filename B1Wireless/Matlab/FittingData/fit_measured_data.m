%FIT_MEASURED_DATA generates a
% Laguerre fit to a set of measured
% channel data.  The fit mathematically
% describes the probability density
% of the attenuation (squared) of a 
% measured wireless channel.
% 
% jpc 23/10/13
function [ a, best_order, min_ls_error ] = fit_measured_data( maxorder, alpha )
    
% Load the measured data.
% Two vectors will appear
% in the workspace:
%   xi is a vector of data point
%     locations;
%   fo is a vector of data points.
load('measured_data.mat')

% Plot the data.
% This will help you to
% visualisation what the fit
% should look like.
ystr = sprintf('Density data points');
figure
set(gca,'FontSize',18);
plot(xi,fo,'kx','LineWidth',1.2)
xlabel('x');
ylabel(ystr);
grid on

%%% Your code here %%%

% estimate the mean of the data
mean_estimate = trapz(xi, fo.*xi);

% estimate the variance
sigma2_estimate = trapz(xi, (xi.^2).*fo) - mean_estimate^2;

% create new data points
x_corrected = 2*(xi)/sigma2_estimate^0.5;

% do a fit for each order and pick the fit that minimises the mean square
% error
min_ls_error = 0;
best_order = 0;
a = [];
for n = 0:maxorder
    % do the Laguerre fit
    fit = fit_laguerre(x_corrected, fo, n, alpha);
    
    % calculate the least squares error
    error = least_square_error(fit, fo, xi);
    
    % check if the fit is the best so far, if so save the order
    if n == 1 || error < min_ls_error
        min_ls_error = error;
        best_order = n;
        a = fit;
    end
end

% plot the results
hold all;
plot(xi, a);
legend('Original data', sprintf('Best Laguerre fit (n=%d)', best_order));
title(sprintf('Histogram points for measured data, fit a=%d, n=%d', alpha, best_order));

end

