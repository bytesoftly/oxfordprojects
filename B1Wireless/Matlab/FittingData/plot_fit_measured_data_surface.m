%PLOT_FIT_MEASURED_DATA Demonstrates usage of fit_measured_data

% include required lib
addpath('../Laguerre');

% maximum order to create a fit with
maxorder = 50;

% maximum alpha value to use
max_alpha = 30;

% number of alpha values to try
alpha_num = 100;

% do the fit
[a, best_order, best_alpha, min_ls_error] = fit_measured_data_surface(maxorder, max_alpha, alpha_num);