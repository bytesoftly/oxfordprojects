%PLOT_FIT_MEASURED_DATA Demonstrates usage of fit_measured_data

% include required lib
addpath('../Laguerre');

% maximum order to create a fit with
maxorder = 100;

% do the fit
[a, best_order, best_alpha, min_ls_error] = fit_measured_data_smart(maxorder);