%PLOT_FIT Demonstrates Laguerre fitting with some randomly generated sample
%data

% include required lib
addpath('../Laguerre');

% variance
sigma2 = 2;

% mean
mu = 0;

% number of samples
nsamp = 1e4;

% number of bins
nbins = 100;

% order of Laguerre fit
n = 0;

% parameter of Laguerre fit
a = 0;

% generate the data
[fo, x] = exp_data(sigma2, mu, nsamp, nbins);

% create the Laguerre fit
fit = fit_laguerre(x, fo, n, a);

% calculate the data mean
fo_m = trapz(x, x.*fo);

% calculate a parameterised chi fit
chi_fit = chi_param_fit(x, fo_m);

% calculate the least square error of each method
chi_error = least_square_error(fo, chi_fit, x);
lag_error = least_square_error(fo, fit, x);

% report the errors
fprintf('Laguerre fit error=%f\n', lag_error);
fprintf('Chi fit error=%f\n', chi_error);
if lag_error < chi_error
    disp('Laguerre fit was better');
else
    disp('Chi fit was better');
end

% plot both the actual data and the fit on the same graph
plot(x, fo, 'kx', 'LineWidth', 0.6);
hold all;
plot(x, fit, 'LineWidth', 1.2);
hold all;
plot(x, chi_fit, 'LineWidth', 1.2);
title(sprintf('Laguerre and parameterised CHI fit to data, order=%d, a=%d', n, a));
grid on;
xlabel('x');
legend('Original data', 'Laguerre fit', 'CHI fit');

% save the diagram into FigImages folder
print('-dpng', sprintf('../../FigImages/fit_laguerre_a%d_n%d.png', a, n));
