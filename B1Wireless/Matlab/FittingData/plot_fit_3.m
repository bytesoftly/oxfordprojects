%PLOT_FIT_3 Demonstrates Laguerre fitting of different orders with scaled
%input

% include required lib
addpath('../Laguerre');

% whether or not the use scaled values
use_scaled = false;

% variance
sigma2 = 6;

% mean
mu = 0;

% number of samples
nsamp = 1e4;

% number of bins
nbins = 100;

% orders of Laguerre fit
n = 0:5;

% Laguerre parameter to use
a = 0;

% generate the random data to fit
[fo, x] = exp_data(sigma2, mu, nsamp, nbins);

% scale the data
xscaled = 2*x/sigma2;

% decide whether or not to use scaled values
if use_scaled
    x = xscaled;
end

% plot the data
if use_scaled
    plot(x, fo, 'kx', 'DisplayName', 'Scaled original data');
else
    plot(x, fo, 'kx', 'DisplayName', 'Original data');
end

% create a lagurre fit for each parameter value
for l = 1:length(n)
    % create the Laguerre fit
    fit = fit_laguerre(x, fo, n(l), a);

    % calculate the least squares error
    error = least_square_error(fit, fo, x);
    fprintf('Least squares error for a=%d is %f\n', a, error);

    % plot the fit
    hold all;
    plot(x, fit, 'DisplayName', sprintf('Lageurre n=%d fit, error=%f', n(l), error));
end

xlabel('x');
if use_scaled
    title(sprintf('Laguerre (up to order %d) fits for zero mean scaled data', max(max(n))));
else   
    title(sprintf('Laguerre (up to order %d) fits for zero mean data', max(max(n))));
end
legend(gca, 'show');
grid on;

% save the diagram into FigImages folder
if use_scaled
    print('-dpng', '../../FigImages/fit_laguerre_3_scaled.png');
else
	print('-dpng', '../../FigImages/fit_laguerre_3.png');
end

% do a chi param fit too
estimated_mean = trapz(x,x.*fo);
chi_fit = chi_param_fit(x, estimated_mean);
figure;
plot(x,fo);
hold all;
plot(x, chi_fit);
grid on;
title(sprintf('Chi-square param=%f fit',estimated_mean));
legend('Original data', 'Chi fit');
xlabel('x');
% save the diagram into FigImages folder
if use_scaled
    print('-dpng', '../../FigImages/fit_chi_3_scaled.png');
else
	print('-dpng', '../../FigImages/fit_chi_3.png');
end