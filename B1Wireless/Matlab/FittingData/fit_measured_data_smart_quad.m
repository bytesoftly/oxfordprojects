%FIT_MEASURED_DATA generates a
% Laguerre fit to a set of measured
% channel data.  The fit mathematically
% describes the probability density
% of the attenuation (squared) of a 
% measured wireless channel.
% 
% jpc 23/10/13
function [ a, best_order, best_alpha, min_ls_error ] = fit_measured_data_smart_quad( maxorder, max_alpha, alpha_num, iterations )
    
% Load the measured data.
% Two vectors will appear
% in the workspace:
%   xi is a vector of data point
%     locations;
%   fo is a vector of data points.
load('measured_data.mat')

%%% Your code here %%%

% estimate the mean of the data
mean_estimate = trapz(xi, fo.*xi);

% estimate the variance
sigma2_estimate = trapz(xi, (xi.^2).*fo) - mean_estimate^2;

% create new data points
x_corrected = 2*(xi)/sigma2_estimate^0.5;

% do a fit for each order and pick the fit that minimises the mean square
% error
min_cost = 0;
min_ls_error = 0;
best_order = 0;
best_alpha = 0;
a = [];

% setup initial alpha/order ranges to test
current_max_alpha = max_alpha;
current_min_alpha = 0;
current_max_order = maxorder;
current_min_order = 0;

current_iteration = 0;

while current_iteration < iterations
    % generate some alpha values
    all_alpha = linspace(current_min_alpha, current_max_alpha, alpha_num);

    % generate some orders
    all_orders = current_min_order:current_max_order;

    % preallocate for speed
    order_num = length(all_orders);
    costs = zeros(order_num, alpha_num);   
    
    % sum of least square error in each quadrant
    quad_scores = [0 0; 0 0];
    current_min_cost = 0;
    
    % calculate the error space
    for k = 1:order_num
        n = all_orders(k);
        
        % calculate which side of the order graph we're in (outputs 1 or 2)
        n_quad = round(k/order_num) + 1;
        
        for j = 1:alpha_num
            alpha = all_alpha(j);
            
            % calculate which side of the alpha graph we're in
            alpha_quad = round(j/alpha_num) + 1;

             % do the Laguerre fit
            fit = fit_laguerre(x_corrected, fo, n, alpha);

            % calculate the least squares error
            error = least_square_error(fit, fo, xi);
            cost = calc_cost(error, n);
            costs(k, j) = cost;
            
            % add it to the corresponding quadrant total
            %quad_scores(n_quad, alpha_quad) = quad_scores(n_quad, alpha_quad) + error;
            
            % check if this is the best so far for this surface
            if (j == 1 && k == 1) || cost < current_min_cost
                % set this as the best quad (this is a hack applied to the previous approach using averages)
                quad_scores = [0 0; 0 0];
                quad_scores(n_quad, alpha_quad) = -1;
                current_min_cost = cost;
            end

            % check if the fit is the best so far, if so save the order and
            % alpha
            if (n == 0 && j == 1) || cost < min_cost
                min_cost = cost;
                min_ls_error = error;
                best_alpha = alpha;
                best_order = n;
                a = fit;
                fprintf('New best alpha found, n=%d, a=%f (error=%f) iteration %d\n', best_order, best_alpha, error, current_iteration);
            end
        end
    end
    
    % check if we've gone as fine as we can go
    if current_max_order == current_min_order
        disp('Settled on an order, stopping');
        fprintf('Settled on order=%d after %d iterations\n', best_order, current_iteration);
        break;
    end
    
    % pick the quadrant of the surface with the lowest total error sum and
    % set a new alpha/order range to test
    quad_scores = quad_scores == min(min(quad_scores));
    
    % adjust the order limits (set the center as a max or min) if not
    % already settled
    if current_max_order ~= current_min_order
        order_centre = round(0.5*(current_max_order - current_min_order)) + current_min_order;
        if quad_scores(1,1) || quad_scores(1,2)
            current_max_order = order_centre;
        else
            current_min_order = order_centre;
        end
    end
    
    % adjust the alpha limits (set the center as a max or min)
    alpha_centre = 0.5*(current_max_alpha - current_min_alpha) + current_min_alpha;
    if quad_scores(1,1) || quad_scores(2,1)
        current_max_alpha = alpha_centre;
    else
        current_min_alpha = alpha_centre;
    end
    
    % increment the iteration count
    current_iteration = current_iteration + 1;
    
    % plot the result (for debugging)
    if current_max_order ~= current_min_order
        print_plot_error(all_alpha, all_orders, costs, current_iteration);
        drawnow;
    end
end

% plot the final result
%plot_error(all_alpha, all_orders, errors);

% plot as a surface, this assumes equal increments of orders and alphas
%errors = reshape(errors, maxorder+1, alpha_num);
% plot the results
ystr = sprintf('Density data points');
figure;
set(gca,'FontSize',18);
plot(xi,fo,'kx','LineWidth',1.2)
xlabel('x');
ylabel(ystr);
grid on
hold all;
plot(xi, a);
legend('Original data', sprintf('Best Laguerre fit (n=%d, a=%.2f)', best_order, best_alpha));
title(sprintf('Histogram points for measured data, fit a=%.2f, n=%d', alpha, best_order));
print('-dpng', '../../FigImages/fit_measured_data_smart_quad.png');

% TODO: Use an approach of always picking the quadrant with the lowest mean
% and keep going until an arbitrarily small decrement in error is reached.
% Possibly cache parts of the laguerre function generation to save time.

end

function print_plot_error(alphas, orders, errors, iteration)
%PLOT_ERROR Plots a log of the errors and labels/titles graph

%surf(alphas, orders, log(errors));
surf(alphas, orders, errors);
xlabel('alpha');
ylabel('order');
zlabel('least squared error (log)');
title(sprintf('Surface of fit error as a function of fit order and alpha, iteration %d (JMC)', iteration));

% save the diagram into FigImages folder
print('-dpng', sprintf('../../FigImages/quad_fit_laguerre_it%d.png', iteration));

end

function [ cost ] = calc_cost(error, order)
%CALC_COST returns a cost value as a function of the least squared error
%and order of fit. Cost function treats order exponentially to represent
%exponential increase of processing power required to calculate it.

if order == 0
    cost = error;
else
    cost = error + exp(0.0001*order) - 1;
end

end
