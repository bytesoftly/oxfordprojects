function [ error ] = least_square_error(f, fo, x)
%LEAST_SQUARE_ERROR Calculates least square error of f from fo for the
%given x values

error = trapz(x, (f-fo).^2);

end

