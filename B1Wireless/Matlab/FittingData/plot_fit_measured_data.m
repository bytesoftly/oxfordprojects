%PLOT_FIT_MEASURED_DATA Demonstrates usage of fit_measured_data

% include required lib
addpath('../Laguerre');

% maximum order to create a fit with
maxorder = 5;

% Laguerre alpha parameter
alpha = 2;

% do the fit
[a, best_order, min_ls_error] = fit_measured_data(maxorder, alpha);