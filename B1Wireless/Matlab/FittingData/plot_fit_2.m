%PLOT_FIT_2 Demonstrates Laguerre fitting with a set of data best fit with
%a non-zero Laguerre parameter

% include required lib
addpath('../Laguerre');

% variance
sigma2 = 1/3;

% mean
mu = 1 + 1i;

% number of samples
nsamp = 1e4;

% number of bins
nbins = 100;

% orders of Laguerre fit
n = 0:5;

% different Laguerre parameters to use
a = [0 2];

% generate the random data to fit
[fo, x] = exp_data(sigma2, mu, nsamp, nbins);

% plot the data
plot(x, fo, 'kx', 'LineWidth', 1.2, 'DisplayName', 'Original data');

% create a lagurre fit for each parameter value
for l = 1:length(n)
    for j = 1:length(a)
        % create the Laguerre fit
        fit = fit_laguerre(x, fo, n(l), a(j));

        % calculate the least squares error
        error = least_square_error(fit, fo, x);
        fprintf('Least squares error for a=%d is %f\n', a(j), error);

        % plot the fit
        hold all;
        plot(x, fit, 'DisplayName', sprintf('a=%d, n=%d fit, error=%.3f', a(j), n(l), error));
    end
end

xlabel('x');
title('Laguerre fits for different alpha parameters');
legend(gca, 'show');
grid on;

% save the diagram into FigImages folder
print('-dpng', '../../FigImages/fit_laguerre_2.png');