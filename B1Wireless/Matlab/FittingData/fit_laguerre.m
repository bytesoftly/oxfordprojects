function [ fit ] = fit_laguerre(x, fo, n, a)
%FIT_LAGUERRE Creates a Laguerre fit of order n, parameter a at given x
%values to the data fo

% preallocate
fit = zeros(1, length(x));

% generate the required Laguerre function
orth_lag = orthonormal_laguerre(n, a, x);

% calculate the coefficient, scale the orth lag function and superimpose it
for k = 0:n
    fit = fit + inner_product(x, orth_lag(k+1,:), fo)*orth_lag(k+1,:);
end

end

