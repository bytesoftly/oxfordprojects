%FIT_MEASURED_DATA generates a
% Laguerre fit to a set of measured
% channel data.  The fit mathematically
% describes the probability density
% of the attenuation (squared) of a 
% measured wireless channel.
% 
% jpc 23/10/13
function [ a, best_order, best_alpha, min_ls_error ] = fit_measured_data_smart( maxorder )
    
% Load the measured data.
% Two vectors will appear
% in the workspace:
%   xi is a vector of data point
%     locations;
%   fo is a vector of data points.
load('measured_data.mat')

% Plot the data.
% This will help you to
% visualisation what the fit
% should look like.
ystr = sprintf('Density data points');
figure
set(gca,'FontSize',18);
plot(xi,fo,'kx','LineWidth',1.2)
xlabel('x');
ylabel(ystr);
grid on

%%% Your code here %%%

% estimate the mean of the data
mean_estimate = trapz(xi, fo.*xi);

% estimate the variance
sigma2_estimate = trapz(xi, (xi.^2).*fo) - mean_estimate^2;

% create new data points
x_corrected = 2*(xi)/sigma2_estimate^0.5;

% do a fit for each order and pick the fit that minimises the mean square
% error
min_ls_error = 0;
best_order = 0;
a = [];

% find the best order for a non-zero alpha
alpha = 1;
for n = 0:maxorder
    % do the Laguerre fit
    fit = fit_laguerre(x_corrected, fo, n, alpha);
    
    % calculate the least squares error
    error = least_square_error(fit, fo, xi);
    
    % check if the fit is the best so far, if so save the order
    if n == 1 || error < min_ls_error
        min_ls_error = error;
        best_order = n;
        a = fit;
        fprintf('New best order found, n=%d (error=%f)\n', best_order, error);
    end
end

% find the best alpha for the given order
all_alpha = 1:0.1:20;
best_alpha = 0;
for alpha = all_alpha
     % do the Laguerre fit
    fit = fit_laguerre(x_corrected, fo, best_order, alpha);
    
    % calculate the least squares error
    error = least_square_error(fit, fo, xi);
    
    % check if the fit is the best so far, if so save the order
    if alpha == all_alpha(1) || error < min_ls_error
        min_ls_error = error;
        best_alpha = alpha;
        a = fit;
        fprintf('New best alpha found, a=%f (error=%f)\n', best_alpha, error);
    end
end

% plot the results
hold all;
plot(xi, a);
legend('Original data', sprintf('Best Laguerre fit (n=%d, a=%f)', best_order, best_alpha));
title(sprintf('Histogram points for measured data, fit a=%d, n=%d', alpha, best_order));

end

