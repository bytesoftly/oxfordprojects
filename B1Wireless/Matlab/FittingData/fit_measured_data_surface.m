%FIT_MEASURED_DATA generates a
% Laguerre fit to a set of measured
% channel data.  The fit mathematically
% describes the probability density
% of the attenuation (squared) of a 
% measured wireless channel.
% 
% jpc 23/10/13
function [ a, best_order, best_alpha, min_ls_error ] = fit_measured_data_surface( maxorder, max_alpha, alpha_num )
    
% Load the measured data.
% Two vectors will appear
% in the workspace:
%   xi is a vector of data point
%     locations;
%   fo is a vector of data points.
load('measured_data.mat')

%%% Your code here %%%

% estimate the mean of the data
mean_estimate = trapz(xi, fo.*xi);

% estimate the variance
sigma2_estimate = trapz(xi, (xi.^2).*fo) - mean_estimate^2;

% create new data points
x_corrected = 2*(xi)/sigma2_estimate^0.5;

% do a fit for each order and pick the fit that minimises the mean square
% error
min_ls_error = 0;
best_order = 0;
a = [];

% generate some alpha values
all_alpha = linspace(0, max_alpha, alpha_num);
best_alpha = 0;

% generate some orders
all_orders = 0:maxorder;

% preallocate for speed
errors = zeros(maxorder+1, alpha_num);

for n = all_orders
    for j = 1:alpha_num
        alpha = all_alpha(j);
        
         % do the Laguerre fit
        fit = fit_laguerre(x_corrected, fo, n, alpha);

        % calculate the least squares error
        error = least_square_error(fit, fo, xi);
        
        errors(n+1, j) = error;

        % check if the fit is the best so far, if so save the order and
        % alpha
        if n == 0 || error < min_ls_error
            min_ls_error = error;
            best_alpha = alpha;
            best_order = n;
            a = fit;
            fprintf('New best alpha found, n=%d, a=%f (error=%f)\n', best_order, best_alpha, error);
        end
    end
end

% plot as a surface, this assumes equal increments of orders and alphas
%errors = reshape(errors, maxorder+1, alpha_num);
surf(all_alpha, all_orders, log(errors));
xlabel('alpha');
ylabel('order');
zlabel('least squared error (log)');
title('Surface showing fit error as a function of fit order and alpha (JMC)');

% TODO: Use an approach of always picking the quadrant with the lowest mean
% and keep going until an arbitrarily small decrement in error is reached.
% Possibly cache parts of the laguerre function generation to save time.

end

