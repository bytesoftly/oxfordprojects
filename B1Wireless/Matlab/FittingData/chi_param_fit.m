function [ fit ] = chi_param_fit(x, param)
%CHI_PARAM_FIT Creates a CHI-SQUARE parameterised fit from the given x
%values and param

fit = exp(-1*x/param)/param;

end

