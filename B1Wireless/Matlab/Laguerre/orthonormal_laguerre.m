function [ y ] = orthonormal_laguerre(n,a,x)
%ORTHONORMAL_LAGUERRE Calculates numeric solutions from orthonormal
%associated Laguerre functions given order, alpha parameter and x values

% preallocate space for the output values (order n means n+1 Laguerre
% functions
y = zeros(n+1,length(x));

% calculate the associated Laguerre polynomial coefficients
coeffs = fliplr(calc_all_laguerre_coeffs(n, a));

% loop through each function calculating values
for j = 0:n
    % calculate the scaling factor from the gamma function (using matlab's
    % built in gamma function)
    gamma_scale = (gamma(j+1)/gamma(j+a+1))^0.5;
    poly_vals = polyval(coeffs(j+1,:),x);
    y(j+1,:) = gamma_scale.*poly_vals.*(x.^(a/2)).*exp(-0.5*x);
end

end

