%PLOT_ORTHONORMAL_LAGUERRE Demonstrates producing orthonormal Laguerre
%functions with orthonormal_laguerre, also tests for orthonormality

% define the alpha parameters to use
a = [0, 2];

% define the order to calculate up to (inclusive)
n = 5;

% create some x values to use
x = linspace(0, 40, 1000);
x=0:0.1:40;

% create a plot for every alpha value specified
orthonormal = true;
for j = 1:length(a)
    % calculate the values
    w = orthonormal_laguerre(n, a(j), x);
    
    % verify all are orthonormal
    for k = 1:n+1
        % check the magnitude
        mag = inner_product(x, w(k,:), w(k,:));
        if (abs(mag-1) < 10^2)
            mag = 1;
        else
            orthonormal = false;
        end
        
        % check it's orthogonal with all previous ones
        orth = true;
        if k > 1
            for l = 1:k-1
                inner = inner_product(x, w(k,:), w(l,:));
                if (abs(inner) > 10^-2)
                    orth = false;
                    orthonormal = false;
                    break;
                end
            end
        end
        
        if orth
            fprintf('Function %d is orthogonal with norm=%f\n', k-1, mag);
        else
            fprintf('Function %d is not orthogonal with norm=%f\n', k-1, mag);
        end
    end

    if orthonormal
        disp('All functions orthonormal');
    else
        disp('Functions are not all orthonormal');
    end
    
    % plot the values
    figure;
    for row = 1:size(w, 1)
        plot(x, w(row, :), 'DisplayName', sprintf('order %d', row-1));
        hold all;
    end
    legend(gca, 'show');
    title(sprintf('Orthonormal Laguerre functions to the %dth order with a=%d', n, a(j)));
    grid on;
    
    % save the diagram into FigImages folder
    print('-dpng', sprintf('../../FigImages/orthonormal_laguerre_a%d_n%d.png', a(j), n));
end

   