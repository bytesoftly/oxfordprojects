% PLOT_ALL_LAGUERRE Plots Laguerre polynomials over a range, demonstrating
% the calc_all_laguerre_coeffs function
% James Clemoes 4/1/2016

% Number of polynomials to plot
N = 5;

% calculate the polynomial coefficients
coeffs = calc_all_laguerre_coeffs(N,0);

% specify a range of x values to plot over
x = linspace(-5, 20, 100);

for n = 1:N+1
    plot(x,polyval(fliplr(coeffs(n,:)), x));
    hold on;
end
grid on;
title(sprintf('Leguerre polynomials of order 0 to %d (JMC)', N));
xlabel('x');
ylabel('L_{0}(x)');
axis([-5 20 -10 20]);

% save the diagram into FigImages folder
print('-dpng', sprintf('../../FigImages/laguerre_a0_n%d.png', N));