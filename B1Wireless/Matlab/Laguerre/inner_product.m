function [ y ] = inner_product( x, a, b )
%INNER Calculates the inner product of a and b via the trapezium rule at
%every x point

y = trapz(x,a.*b);

end

