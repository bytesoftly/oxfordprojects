function [ coeffs ] = calc_laguerre_coeffs(n, a)
%CALC_LAGUERRE_COEFFS Calculates coefficients of the Rodrigues' formula for
%the nth order Laguerre polynomial. Polynomial coefficients are given in
%ascending order of x.
% James Clemoes 3/1/2016

% allocate space for the coefficients
coeffs = zeros(1, n+1);

for k = 0:n
    [coeff, x_power] = calc_one_coeff(n, a, k);
    coeffs(x_power+1) = coeff;
end

% reverse order of coefficients to ascending powers of x (this done to
% match the notes but is arbitrary)
coeffs = fliplr(coeffs);

% scale the coefficients by the factorial term in Rodrigues' formula
coeffs = coeffs/factorial(n);

end

function [ coeff, x_order ] = calc_one_coeff(n, a, k)
%CALC_ONE_COEFF Calculates the coefficent of one power of x for the
%Laguerre polynomial. Returns the coefficient and the power of x it should
%multipy

x_order = n-k;                 % order of x this coefficient multiplies
x_coeff = prod(a+n-(0:k-1));   % determines the magnitude
e_coeff = (-1)^(n-k);          % determines the sign
coeff = nchoosek(n, k)*x_coeff*e_coeff;

end