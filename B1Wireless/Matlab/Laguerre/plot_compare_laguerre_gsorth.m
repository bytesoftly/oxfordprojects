%PLOT_COMPARE_LAGUERRE_GSORTH Plots the difference between an orthonormal
%set of function generated previously via Gram Schmidt with the Laguerre
%polynomials

% add path to Gram Schmidt code
addpath('../GramSchmidt');

% number of polynomials to compare
N = 6;

% create a range of x values to use
x = linspace(0, 20, 100);

% calculate the laguerre polynomial coefficients, hence values
lag_coeffs = fliplr(calc_all_laguerre_coeffs(N-1,0));
lag = zeros(N, length(x));
for j = 1:N
    lag(j,:) = polyval(lag_coeffs(j,:),x);
end

% calculate the orthonormal set via Gram Schmidt
[~, v] = gram_schmidt(N,x);

% calculate the difference between both outputs
v_diff = lag - v;

% plot all three results
ax1 = subplot(2,1,1);
plot(x, lag);
title('Laguerre polynomials');

xlim([8 20]);

ax2 = subplot(2,1,2);
plot(x, v);
title('Gram Schmidt polynomial');

xlim([8 20]);

%ax3 = subplot(2,1,3);
%plot(x, v_diff);
%title('Difference between an orthonormal set and Laguerre polynomials (JMC)');
%linkaxes([ax1,ax2,ax3],'xy');

% save the diagram into FigImages folder
print('-dpng', '../../FigImages/orthonormal_laguerre_vs_gramschmidt.png');