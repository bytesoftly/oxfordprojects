function [ coeffs ] = calc_all_laguerre_coeffs(n, a)
%CALC_ALL_LAGUERRE_COEFFS Successively calculates coefficients for
%Laguerre polynomials up to the nth order
% James Clemoes 4/1/2016

% preallocate space (room for n+1 sets of vectors of n+1 coefficients,
% ascending powers of x - to remain consistant with the notes altough
% requires flipping down the line to use polyval(...))
coeffs = zeros(n+1);

% fill in the first two known sets of coefficients
coeffs(1,1) = 1;
coeffs(2, 1:2) = [1+a, -1];

% build up the rest
for k = 2:n
    % super impose the coefficients from the last two polynomials as
    % required (multiplying two polynomials is equivalent to calculating
    % the convolution of vectors of their coefficients) and scale
    prev_poly = [2*k+a-1 -1];
    coeffs(k+1, 1:k+1) = (conv(prev_poly, coeffs(k, 1:k)) - (k+a-1)*coeffs(k-1, 1:k+1))/k;
    %Note: row index is one more than expected (as matlab is one based)
    %Note: convolution increments order of polynomial by 1 so one extra
    %column is needed from the -2 polynomial coefficients. Can't simply
    %conv an entire row as will get additional unrequired zero padding in
    %the solution.
end

end