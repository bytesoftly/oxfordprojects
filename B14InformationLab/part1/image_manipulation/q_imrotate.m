q_imload;

% rotate the image
J = imrotate(I, 35, 'bilinear');

% show the result
imshow(J);