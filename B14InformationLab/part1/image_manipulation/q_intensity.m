q_imload;

% plot the intensity distribution of an ROI. (region of interest)
figure
imshow(I)
subIm = imcrop;
imshow(subIm);
J = double(subIm)/255; % convert to floating point format in range 0 to 1
mesh(J,'EdgeColor','red');
hidden off; % turn off hidden line removal (make mesh transparent)
hold on; 
warp(J); 
hold all;
warp(J,J);
hold off;