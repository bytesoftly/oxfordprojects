% load the image
q_imload;

figure
dim = size(I);
I_d = double(I) / 255;
subplot(2,3,1); imshow(I_d);

% Compute a windowing function.
h = hanning(128);
h1 = [h(1:64) ; ones(dim(1)-128,1) ; h(65:128)];
h2 = [h(1:64) ; ones(dim(2)-128,1) ; h(65:128)];
hwind = h1*h2';

% Compute the FFT
spec = fftshift(fft2(I_d .* hwind));

% Compute the abs log spectrum
logsp = log(abs(spec));
subplot(2,3,2);
imagesc(logsp);

% Apply a Laplacian-of-Gaussian filter
K = fspecial('log',11,3);
fspec = -filter2(K,logsp,'same');
subplot(2,3,3);
imagesc(fspec);

% Threshold to find the peaks
sigma = std2(fspec);
thresh = fspec > 2.5*sigma;

% Make sure the central peak isn�t zeroed
centre_x = floor(dim(2)/2);
centre_y = floor(dim(1)/2);
thresh(centre_y-10:centre_y+10, centre_x-10:centre_x+10) = 0;
subplot(2,3,4);
imagesc(thresh);

% Perform morphological erosion/dilation
thresh = bwmorph(thresh,'erode');
thresh = bwmorph(thresh,'dilate');
subplot(2,3,5);
imagesc(thresh);

% Zero the corresponding components in the FFT
spec(thresh == 1) = 0;

% Finally, inverse FFT
subplot(2,3,6);
imshow(abs(ifft2(spec)));