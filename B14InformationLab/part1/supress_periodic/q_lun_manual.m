% load the image
q_imload;

% calculate and show the spectrum
Ifft = fftshift(fft2(I));
figure; 
colormap(gray);
imagesc(log(abs(Ifft) + 1));

% get the input of the peaks (manually)
[x,y] = ginput; % Press enter to finish picking peaks

peaks = zeros(size(Ifft));
for i=1:size(x)
% set values at (x,y) to unity
peaks(round(y(i)),round(x(i))) = 1;
end

Ifft(peaks == 1) = 0; % use peaks to set values at (x,y) to zero
imagesc(log(abs(Ifft) + 1)); % look at modified fft

% show the result
figure; colormap(gray);
imagesc(abs(ifft2(Ifft)));

temp_mask = ones(1, 10);
mask = temp_mask' * temp_mask;
temp = filter2( mask, peaks);
Ifft(temp >= 1) = 0;
figure; colormap(gray);
imagesc(log(abs(Ifft) + 1)); % look at modified fft
figure; colormap(gray);
imagesc(abs(ifft2(Ifft))); % look at modified image

