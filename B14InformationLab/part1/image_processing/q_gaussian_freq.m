q_imload;

% add noise to the image
J=imnoise(I, 'salt & pepper');

tic
% compute the 2D Fourier transform
Ifft = fft2(I); 

% do frequency domain filtering
gaufft = fft2(fspecial('gaussian',size(I),2));

% flip the quadrants back
IgauFou = fftshift(real(ifft2( gaufft .* Ifft )));
toc

% display the result
figure;
imagesc(IgauFou); colormap gray;
title('Frequency domain filtering');