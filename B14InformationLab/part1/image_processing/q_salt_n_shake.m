% load the image
q_imload;

% add noise to the image
J=imnoise(I, 'salt & pepper');
imshow(J);

% apply a median filter
K=medfilt2(J,[3 3]); % 3x3 median filtering
figure; % display result on new figure
imshow(K);

% plot the difference between the filtered image and noisy image
figure;
imshow(abs(double(K) - double(J)) > 2);
title('Filtered - noisey');

% plot the difference between the filtered and orginal
figure;
imshow(abs(double(K) - double(I)) > 2);
title('Filtered - original');
