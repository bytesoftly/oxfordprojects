disp('Spatial');
q_gaussian_spatial;
disp('Freq');
q_gaussian_freq;

% show the difference between the two techniques
figure;
imshow(abs(double(IgauFou) - double(IgauSep)));
title('Gaussian spatial vs frequency filtering');