q_imload;

% add noise to the image
J=imnoise(I, 'salt & pepper');

tic
% filter in the x direction
gau_x = fspecial('gaussian',[1,20],2);
Igau_x = filter2( gau_x, I);

% filter in the y direction
gau_y = fspecial('gaussian',[20,1],2);
IgauSep = filter2( gau_y, Igau_x );
toc;

% display the result
imagesc(IgauSep); colormap gray;
title('Seperable spatial');