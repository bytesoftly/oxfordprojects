q_imload;

% add noise to the image
J=imnoise(I, 'salt & pepper');

h = fspecial('gaussian', 13, 2); % default 3x3 filter (sigma=0.5)
h % prints out filter template
L = filter2(h,J); % filters image J with h
figure;
imagesc(L); % displays a float image
colormap gray; % sets colour for display