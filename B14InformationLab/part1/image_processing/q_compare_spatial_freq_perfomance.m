% load an image
q_imload;

% choose a range of standard deviation to try
sig_range = 1:100;

% arrays of time taken
spatial_time = [];
freq_time = [];

% loop through timing the differences
for sig = sig_range
    % spatial filtering
    tic; 
    gau_x = fspecial('gaussian',[1,6*sig+1],sig);
    Igau_x = filter2( gau_x, I );
    gau_y = fspecial('gaussian',[6*sig+1,1],sig);
    IgauSep = filter2( gau_y, Igau_x ); 
    spatial_time(end+1) = toc;
    
    % frequency filtering
    tic; 
    Ifft = fft2(I);
    gaufft = fft2(fspecial('gaussian',size(I),sig));
    IgauFou = fftshift(real(ifft2( gaufft .* Ifft ))); 
    freq_time(end+1) = toc;
end

% plot the results
plot(sig_range, spatial_time);
hold all;
plot(sig_range, freq_time);
title('Comparison of time taken to filter by frequency or spatially');
legend('Spatial twice in 1D', 'Frequency');

% find the crossover
[val, idx] = min(abs(spatial_time - freq_time));
fprintf('Frequency filtering becomes faster at sigma=%d\n', sig_range(idx(1)));