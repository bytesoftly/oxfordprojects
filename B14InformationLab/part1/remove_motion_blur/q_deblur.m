% load the image
q_imload;

% rotate the image to make the motion blur horizontal (to simplify filter)
correction_rotation = -18; % 18 degrees out
J = imrotate(I, correction_rotation);

% crop the image to the number plate
imshow(J);
sub = imcrop;

% estimate the length of the blur
imshow(sub);
[x, ~] = ginput(2);
L = x(2) - x(1);

% create a deblurring filter mask
bar = zeros(size(sub));
[sy,sx] = size(sub);
bar(round(sy/2), round(sx/2 + [-L/2:L/2]) ) = 1/L;
imagesc(bar); 
colormap gray;

% create the filter in the frequency domain
K = 0.02;
barfft = fft2(bar);
Wfil = conj(barfft) ./ (abs(barfft) .* abs(barfft) + K);

% apply the filter in the frequency domain
subfft = fft2(sub);
deblur = real(ifft2( subfft .* Wfil ));
filtered_img = fftshift(deblur);

% apply histogram equalisation
%filtered_img = histeq(filtered_img, );

% show the output
imagesc(filtered_img);

f_scaled = filtered_img - min(min(filtered_img)); % since min(..) < 0
f_scaled = f_scaled/max(max(f_scaled));

% do the histogram correction (to up the contrast in the interesting
% region)
contrasted = histeq(f_scaled);

% plot the final outout
imagesc(contrasted);

% show the cumsum of the histogram
figure;
plot(cumsum(hist(contrasted(:))));
