% mean 
mu = [ 5 ; 2];

% covariance matrix
sigma_x  = 2; 		sigma_xy = 0;
sigma_yx = sigma_xy;	sigma_y  = 1;
C = [ sigma_x^2 	sigma_xy;
      sigma_yx		sigma_y^2;
];

% number of samples
N_range = 10:500:10^5;
M1 = zeros(1, length(N_range));
M2 = zeros(1, length(N_range));

for j = 1:length(N_range)
    N = N_range(j);
    
    % generate point samples
    pts = gaussian_sensor(mu,C,N);

    %
    % Measure moments of distribution
    %

    % the mean
    M1(j) = sum((mean(pts,2) - mu).^2)^0.5;

    % the second moment matrix
    %M2(j) = sum(sum((cov(pts') - C).^2))^0.5;
    M2(j) = norm(abs(cov(pts') - C));
end

figure;
plot(N_range, M1);
hold all
plot(N_range, M2);
title('Variation of least squares error of mean and variance with sample number');