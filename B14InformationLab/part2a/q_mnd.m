% test the normal2d function
x = -5:0.1:5;
y = x;
z = normal2d(x,y, [1 1]', [1 0; 0 4]);

% show some contours
contour(x,y,z);

% do a surface
figure;
surf(x,y,z);

% check if normalised (look it's not one but is 0.1*0.1 out)s
total = sum(sum(z))