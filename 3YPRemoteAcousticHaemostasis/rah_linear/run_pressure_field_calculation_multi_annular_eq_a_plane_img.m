%RUN_PRESSURE_FIELD_CALCULATION_ANNULAR_EQUAL_AREA Demonstrates the rah_linear class
%with a multi-element annular transducer
disp(datestr(now));

% instantiate rah_linear class
obj = rah_linear;

% set the simulation size 10cm * 10cm with 30 steps in xy directions, 10 in
% z direction

x_coords = linspace(-0.01, 0.01, 20);
y_coords = zeros(1, 20);
z_coords = linspace(0.07, 1, 60);
obj.set_sim_size(x_coords, y_coords, z_coords);
obj.set_sim_plane_ahead_size(x_coords, z_coords)

% focal depth to be naturally focussed on (with no phase focussing). This
% sets a bound for the minimum focal depth.
prefocus_depth = 0;

% use an annular transducer 10cm * 10cm with 400 steps of integration in 
%both directions.
obj.create_multi_annular_equal_area_transducer(0.10, 6, 0.020, 0.050, 0.006, 0.0006, 300, prefocus_depth);

% focus the transducer
for theory_focal_depth = 0:0.1:1
    obj.focus_annular_transducer(theory_focal_depth);

    % set the power of the transducer
    obj.set_annular_power(100);

    % calculate the power emitted
    power = obj.calc_power_emitted();

    % run the simulation
    obj.rayleigh_baffle_piston();
    
    % plot just the plane
    figure;
    obj.plot_pressure_field_plane();
    
    print(sprintf('pf_plane_%d.png', theory_focal_depth*10),'-dpng');
    
    disp(theory_focal_depth/1);
end



save(sprintf('pressure_field_focus_%f.mat', theory_focal_depth))