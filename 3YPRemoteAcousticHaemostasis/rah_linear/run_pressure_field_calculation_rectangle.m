%RUN_PRESSURE_FIELD_CALCULATION_ANULUS Demonstrates the rah_linear class
%with a rectangular transducer

% instantiate rah_linear class
obj = rah_linear;

% set the simulation size 10cm * 10cm with 30 steps in xy directions, 10 in
% z direction
obj.set_sim_size(0.1, 30, 0.1, 30, 0.1, 5);

% use a rectangular transducer 5cm * 5cm with 100 steps in both directions
obj.create_rectangular_transducer(0.05, 100, 0.05, 100);

% run the simulation
obj.rayleigh_baffle_piston();

% plot the result
obj.plot_pressure_field();