%RUN_PRESSURE_FIELD_CALCULATION_ANNULAR_EQUAL_AREA Demonstrates the rah_linear class
%with a multi-element annular transducer
disp(datestr(now));

% instantiate rah_linear class
obj = rah_linear;

% set the simulation size 10cm * 10cm with 30 steps in xy directions, 10 in
% z direction

x_coords = linspace(-0.05, 0.05, 20);
y_coords = zeros(1, 20);
z_coords = linspace(0.00001, 1, 60);
obj.set_sim_size(x_coords, y_coords, z_coords);
obj.set_sim_plane_ahead_size(x_coords, z_coords)

% focal depth to be naturally focussed on (with no phase focussing). This
% sets a bound for the minimum focal depth.
prefocus_depth = 0;

% use an annular transducer 10cm * 10cm with 400 steps of integration in 
%both directions.
scale = 0.1;
width = scale;
n_rings = 10;
r1 = 0.20*scale;
r2 = 0.50*scale;
outer_gap = 0.01*scale;
element_area = 0.0003;
n = 200;
obj.create_multi_annular_equal_area_transducer(width, n_rings, r1, r2, outer_gap, element_area, n, prefocus_depth);

% focus the transducer
theory_focal_depth = 0.1;
obj.focus_annular_transducer(theory_focal_depth);

% set the power of the transducer
obj.set_annular_power(100);

% calculate the power emitted
power = obj.calc_power_emitted();

% run the simulation
obj.use_fresnel_approx = true;
obj.rayleigh_baffle_piston();

% plot the transducer
figure;
obj.plot_transducer();

% plot the result
figure;
obj.plot_pressure_field();
title(sprintf('Theory focal depth: %f, prefocus: %f', theory_focal_depth, prefocus_depth));

% plot just the plane
figure;
obj.plot_pressure_field_plane();
[focal_depth, pressure] = obj.get_focal_point();
fprintf('Focal point: %f, pressure: %fMPa\n', focal_depth, pressure/10^6);