% Demonstrates pressure field calculation directly ahead of the piston

% density of water (kgm^-2)
p = 1000;

% speed of sound through water (ms^-1)
c = 1482;

% frequency of ultrasound (Hz)
f = 1.5e6; % 1.5MHz

% time of simulation
t = 1;

% width of the rectangular element (y axis) in meters
width = 0.25;

% height of the rectangular element (x axis) in meters
height = 0.25;

% maximum depth of field of the simulation (z axis) in meters
max_depth = 1;

% calculate parameters
a = height/2;   % half height
b = width/2;    % half width
w = 2*pi*f;     % angular velocity of wave at element
k = w/c;        % wave number (radm^-1)

% create a set of points to evaluate pressure field at
x = linspace(-a, a, 20);
y = linspace(-b, b, 20);
z = linspace(0, max_depth, 15);

% expand points out to a rectangular cuboid in front of the element
% todo: this is very inefficient, vectorise this
%[x, y, z] = meshgrid(x, y, z);
full_x = [];
full_y = [];
full_z = [];
full_u = [];
for z_i = z
    for y_i = y
        for x_i = x
            full_x(end+1) = x_i;
            full_y(end+1) = y_i;
            full_z(end+1) = z_i;
            full_u(end+1) = exp(-1i*w*t);
        end
    end
end

% run the simulation
%rayleigh_baffle_radial_piston(full_x, full_y, full_z, full_u, k, p, c);
p = real(rayleigh_baffle_piston(full_x, full_y, full_z, full_u, k, p, c));

% show an image of the pressure at the baffled piston
scatter3(full_x,full_y,full_z,20,p,'filled');

xlabel('x');
ylabel('y');
zlabel('z');
cb = colorbar;
cb.Label.String = 'Pressure';