function [ p ] = rayleigh_baffle_piston_radius(r0, z, u, k, p, c)
%RAYLEIGH_BAFFLE_PISTON_RADIUS Estimates the pressure field from a baffled
%circular piston across it's radius for a given depth where:
%   r0 = vector of distances across piston radius to calculate at (m)
%   z = vector of depths of field from piston surface to calculate at (m)
%   u = vector of harmonic surface velocities at r0 points on piston (?)
%   k = wave number (radsm^-1)
%   p = medium material density (kgm^-3)
%   c = speed of sound (ms^-1)

r0_num = length(r0);
z_num = length(z);
u_num = length(u);

% validate input
assert(u_num == r0_num, 'Each r0 value must have a corresponding u value (lengths of u and r0 vectors don''t match)');

% preallocate space for pressure field
p = zeros(z_num, r0_num);

% calculate the integral coefficients once (they only vary with time and u)
coeff = -1i*k*p*c*u*0.5/pi;

% loop for each depth of field
for z_idx = 1:z_num
    % loop over each radius
    for r0_idx = 1:r0_num
        p(z_idx, r0_idx) = coeff(r0_idx)*trapz(r0, exp(1i*k*z(z_idx))/z(z_idx));
    end
end

end

