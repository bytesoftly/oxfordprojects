%RUN_PRESSURE_FIELD_CALCULATION_ANNULAR_EQUAL_AREA Demonstrates the rah_linear class
%with a multi-element annular transducer
disp(datestr(now));

% instantiate rah_linear class
obj = rah_linear;

% set the simulation size 10cm * 10cm with 30 steps in xy directions, 10 in
% z direction

x_coords = linspace(-0.01, 0.01, 20);
y_coords = linspace(-0.01, 0.01, 20);
z_coords = linspace(0.05, 0.11, 60);
obj.set_sim_size(x_coords, y_coords, z_coords);

% focal depth to be naturally focussed on (with no phase focussing). This
% sets a bound for the minimum focal depth.
prefocus_depth = 0.1;

% use an annular transducer 10cm * 10cm with 400 steps of integration in 
%both directions.
obj.create_multi_annular_equal_area_transducer(0.10, 20, 0.020, 0.050, 0.006, 0.00006, 300, prefocus_depth);

% focus the transducer
theory_focal_depth = 0.01;
obj.focus_annular_transducer(theory_focal_depth);

% set the power of the transducer
obj.set_annular_power(100);

% calculate the power emitted
power = obj.calc_power_emitted();

% run the simulation
obj.rayleigh_baffle_piston();

% plot the transducer
figure;
obj.plot_transducer();

% plot the result
figure;
obj.plot_pressure_field();

[X, Y, Z] = meshgrid(x_coords, y_coords, z_coords);

F = scatteredInterpolant([obj.x_coords obj.y_coords obj.z_coords], abs(obj.pf'), 'nearest');
pressure = F(X, Y, Z);

figure 
slice(X, Y, Z, pressure, [0], [0], [0.07]);

xlabel('x')
ylabel('y')
zlabel('z')
zlim([0, max(z_coords)])

colorbar
axis equal

hold on 
image([min(x_coords), max(x_coords)], [min(y_coords), max(y_coords)], obj.flatten_transducer())

save(sprintf('pressure_field_focus_%f.mat', theory_focal_depth))