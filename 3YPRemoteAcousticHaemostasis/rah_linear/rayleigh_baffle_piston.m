function [ pf ] = rayleigh_baffle_piston(x, y, z, t, transducer, scale_x, scale_y, u0, f, p, c, ps)
%RAYLEIGH_BAFFLE_PISTON_AHEAD Estimates the pressure field from a baffled
%transducer centered on the origin at [0,0].
%   x = vector of x values
%   y = vector of y values
%   z = vector of depths of field from piston surface to calculate at (m)
%   t = time of simulation (sec)
%   transducer = matrix of binary values specifiying the transducer shape
%   Size of transducer determines integral resolution
%   scale_x, scale_y = number of meters represented by one unit of
%   transducer in given dimension
%   u0 = maximum value of harmonic surface velocity of piston (units?)
%   f = frequency element is operated at (Hz)
%   p = medium material density (kgm^-3)
%   c = speed of sound (ms^-1)
%   ps = phase shift in rads-1

x_num = length(x);
y_num = length(y);
z_num = length(z);

% validate input
assert((x_num == y_num) & (x_num == z_num), 'x, y and z vectors are of differnt lengths');

% preallocate output for speed
pf = zeros(1, x_num);

% calculate x0 and y0 values
[w, h] = size(transducer);
x0 = scale_x*linspace(-w/2, w/2, w);
y0 = scale_y*linspace(-h/2, h/2, h);

% create a mesh to integrate with
[x0_grid, y0_grid] = meshgrid(x0, y0);

% calculate the angular velocity
w = 2*pi*f;

% calculate the wave number
k = w/c;

for idx = 1:x_num
    % calculate the vector of R values for the given x,y,z coord and x0,y0
    % coordinates
    R = sqrt((x(idx)-x0_grid).^2+(y(idx)-y0_grid).^2+z(idx)^2);
    
    % calculate the integrand
    f = transducer.*exp(1i*k*R)./R;
    
    % calculate the double integral
    pf(idx) = trapz(y0, trapz(x0, f, 2));
end

% calculate the integral coefficients
coeffs = -1i*k*p*c*0.5*u0*exp(-1i*w*t + ps)/pi;

% multiply by the coefficients and dump the imaginary part
pf = real(pf.*coeffs);
    
end

