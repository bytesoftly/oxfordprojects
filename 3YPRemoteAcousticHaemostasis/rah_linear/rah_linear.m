classdef rah_linear < handle
    %RAH_LINEAR An implementation of a linear sound field pressure
    %simulation
    %   The Rayleigh integral is evaluated using the Fresnel approximation
    %   to give an approximate pressure field within wet matter from sound
    %   emitted by a specified transducer
    
    %   Copyright James Clemoes 2016
    
    properties
        % coordinates to run pressure field simulation at
        x_coords = [];
        y_coords = [];
        z_coords = [];
        
        x_steps = 0;
        y_steps = 0;
        z_steps = 0;
        
        % matrix of binary pixels defining a transducer (1 for element, 0
        % for not element)
        transducer = [];
        
        % phase shift of the transducer (s)
        ps = [];
        ps_default = 0;
        
        % radius of each element to use for phase shift focussing
        % estimation (only applies to centered radial transducers)
        ps_center_r = [];
        
        % number of meters represented by one unit of transducer in the x
        % direction
        transducer_scale_x = [];
        
        % number of meters represented by one unit of transducer in the y
        % direction
        transducer_scale_y = [];
        
        % maximum harmonic surface velocity (ms^-1)
        u0 = [];
        u0_default = 1;
        
        % frequency of ultrasound (Hz)
        f = [];
        f_default = 100000;%1.5e6;
        
        % radius of curvature of each transducer
        r_c = [];
        r_c_default = 0;
        
        % density of the medium (kgm^-3)
        p = 1000;
        
        % speed of sound (ms^-1)
        c = 1482;
        
        % time of the simulation
        t = 1;
        
        % calculated pressure field
        pf = [];
        
        % whether or not to use the fresnel approximation to the rayleigh
        % integral
        use_fresnel_approx = false;
    end
    
    methods
        function obj = rah_linear()
            %RAH_LINEAR Constructor
        end
        
        function plot_pressure_field(obj)
            %PLOT_FIELD Plots the pressure field
            scatter3(obj.x_coords, obj.y_coords, obj.z_coords, 30, abs(obj.pf),'filled');
            xlabel('x');
            ylabel('y');
            zlabel('z');
            cb = colorbar;
            cb.Label.String = 'Pressure (Pa)';
            title('Pressure field');
        end
        
        function plot_pressure_field_log(obj)
            %PLOT_FIELD Plots the natural log of the pressure field
            scatter3(obj.x_coords, obj.y_coords, obj.z_coords, 30, log(abs(obj.pf)),'filled');
            xlabel('x');
            ylabel('y');
            zlabel('z');
            cb = colorbar;
            cb.Label.String = 'Natural log of Pressure (Pa)';
            title('Pressure field');
        end
        
        function plot_pressure_field_plane(obj)
            %PLOT_PRESSURE_FIELD_PLANE Plots the pressure field plane when
            %set_sim_size_straight_ahead is used
            
            cpf = obj.get_pressure_field_plane();
            
            % trace the highest pressure location
            idx = find(cpf == max(max(cpf)));
            focal_point = obj.z_coords(idx)
            
            imagesc(flipud(cpf));
            colormap Hot
            %colormap(flipud(colormap));
            title('Pressure field slice');
        end
        
        function [fp, pressure] = get_focal_point(obj)
            %GET_FOCAL_POINT Estimates the focal point when 
            %set_sim_size_straight_ahead is used
            cpf = obj.get_pressure_field_plane();
            
            % trace the highest pressure location
            idx = find(cpf == max(max(cpf)));
            fp = obj.z_coords(idx);
            pressure = cpf(idx);
        end
        
        function [cpf] = get_pressure_field_plane(obj)
            %GET_PRESSURE_FIELD_PLANE Gets the pressure field plane
            %calculated when set_sim_size_straight_ahead is used
            
            cpf = reshape(obj.pf, [obj.z_steps, obj.x_steps]);
        end
        
        function plot_transducer(obj)
            %PLOT_TRANSDUCER Plots the flattened transducer
            imshow(obj.flatten_transducer());
            title('Flattened transducer');
        end
        
        function [ flat_transducer ] = flatten_transducer(obj)
            %FLATTEN_TRANSDUCER Returns a 2D matrix representing all
            %elements of the transducer
            flat_transducer = obj.transducer(:,:,1);
            element_num = obj.get_transducer_element_count();
            
            % check we've got multiple elements to flatten
            if element_num == 1
                return;
            end
            
            % do the flattening
            for j = 2:obj.get_transducer_element_count()
                flat_transducer = flat_transducer + obj.transducer(:,:,j);
            end
            
            % convert back to binary (should do nothing as a valid
            % multi-element transducer shouldn't have overlapping elements)
            flat_transducer = flat_transducer > 0;
        end
        
        function add_transducer(obj, trans_matrix, scale_x, scale_y, phase_shift, ps_center_r, u0, f, r_c)
            %ADD_TRANSDUCER Adds a transducer to the list
            if isempty(obj.transducer)
                obj.transducer = trans_matrix;
            else
                obj.transducer(:,:,end+1) = trans_matrix;
            end
            
            % save the transducer element parameters
            obj.u0(end+1) = u0;
            obj.f(end+1) = f;
            obj.ps(end+1) = phase_shift;
            obj.ps_center_r(end+1) = ps_center_r;
            obj.transducer_scale_x(end+1) = scale_x;
            obj.transducer_scale_y(end+1) = scale_y;
            obj.r_c(end+1) = r_c;
        end
        
        function create_rectangular_transducer(obj, width, width_steps, height, height_steps)
            %CREATE_RECTANGULAR_TRANSDUCER Creates a rectangular transducer
            %of given width, height and steps of integtration in
            %corresponding directions.
            new_trans = ones(width_steps, height_steps);
            scale_x = width/width_steps;
            scale_y = height/height_steps;
            
            % add the transducer element
            obj.add_transducer(new_trans, scale_x, scale_y, obj.ps_default, 0, obj.u0_default, obj.f_default, obj.r_c_default);
        end
        
        function create_annular_transducer(obj, width, r1, r2, n, r_c)
            %CREATE_ANNULAR_TRANSDUCER Creates a circular, annular
            %transducer with given inner and outer radii centered in a
            %square matrix of width/height n and radius of curvature
            scale_x = width/n;
            scale_y = scale_x;
            new_trans = obj.generate_anulus(n, round(r1/scale_x), round(r2/scale_x));
            
            % location to approximate ring at for focussing estimation
            ps_center = (r1+r2)/2;
        
            % add the transducer element
            obj.add_transducer(new_trans, scale_x, scale_y, obj.ps_default, ps_center, obj.u0_default, obj.f_default, r_c);
        end
        
        function focus_annular_transducer(obj, distance)
            %FOCUS_ANNULAR_TRANSDUCER Creates phase offsets to focus and
            %annular transducer straight ahead (only works if all
            %transducer elements are annular, centered, and operate at the
            %same frequency - frequency of the first element is used). The
            %achieved focal depth will be the sum of the parameter distance
            %and the focal depth of the prefocus.
            
            outer_hyp = 0;
            
            % start with the outer ring having zero phase offset
            for r_idx = 1:obj.get_transducer_element_count()
                %todo: Think about how to correct for the z coordinate of
                %the transducer surface
                
                % take transducer surface curvature into account
                %z=0;
                if obj.r_c(r_idx) == 0
                    z = 0;
                else
                    %z0_grid = obj.r_c(t_idx)-sqrt(abs(obj.r_c(t_idx)^2-x0_grid.^2-y0_grid.^2));
                    z = obj.r_c(r_idx)-sqrt(abs(obj.r_c(r_idx)^2-obj.ps_center_r(r_idx)^2));
                end
                wavelength = obj.c/obj.f(r_idx);
                
                % use pythag to do the calculation
                hyp = (((distance-z)^2 + obj.ps_center_r(r_idx)^2)^0.5);
                if outer_hyp == 0
                    obj.ps(r_idx) = 0;
                    outer_hyp = hyp;
                else
                    %dist = outer_hyp - hyp;
                    dist = hyp - outer_hyp;
                    dist = dist;
                    if dist > wavelength
                        warning('Phase focus out of range (required phase offset leads to distance larger than a wavelength)');
                    end
                    obj.ps(r_idx) = dist/obj.c;
                    % check we're still within a range we can effect the
                    % focal point
                    
                end
            end
        end
        
        function [ power ] = calc_power_emitted(obj)
            %CALC_POWER_EMITTED Estimates the total power emitted by all
            %transducer elements
            power = 0;
            for r_idx = 1:obj.get_transducer_element_count()
                % surface pressure approximation
                p_s = obj.p*obj.c*obj.u0(r_idx);
                power_per_sample = p_s*obj.u0(r_idx)*obj.transducer_scale_x(r_idx)*obj.transducer_scale_y(r_idx);
                trans = obj.transducer(:,:,r_idx);
                power = power + sum(trans(:)*power_per_sample);
            end
        end
        
        function set_annular_power(obj, transducer_power)
            %SET_ANNULAR_POWER Sets the power of the transducer by
            %calculating maximum harmonic velocities of specific elements
            %assuming each has equal power
            n_rings = obj.get_transducer_element_count();
            power_per_ring = transducer_power/n_rings;
            for r_idx = 1:n_rings
                trans = obj.transducer(:,:,r_idx);
                area_per_sample = obj.transducer_scale_x(r_idx)*obj.transducer_scale_y(r_idx);
                obj.u0(r_idx) = (power_per_ring/(obj.p*obj.c*sum(trans(:)*area_per_sample)))^0.5;
            end
        end
        
        function create_multi_annular_transducer(obj, width, r1, r2, r_spacing, n_rings, n, r_c)
            %CREATE_MULTI_ANNULAR_TRANSDUCER Creates a multi-element, annular
            %transducer with given inner and outer radii with spacing centered in a
            %square matrix of width/height n and radius of curvature. 
            %Assumes spacing is required on outside of the largest ring 
            %too.
            
            % calculate the width of the rings
            ring_width = (r2 - r1)/n_rings - r_spacing;
            
            r1_current = r1;
            
            % add each ring to the model
            for r_idx = 1:n_rings
                r2_current = r1_current + ring_width;
                obj.create_annular_transducer(width, r1_current, r2_current, n, r_c);
                r1_current = r2_current + r_spacing;
            end
        end
        
        function create_multi_annular_equal_area_transducer(obj, width, n_rings, r1, r2, outer_gap, element_area, n, r_c)
            %CREATE_MULTI_ANNULAR_EQUAL_AREA_TRANSDUCER Creates a
            %multi-element, annular transducer with given inner and outer
            %radii, a gap from the outer most element, the area of each
            %centered in a square matrix of width/height n representing the
            %width/height specified and radius of curvature
            
            % test decrement tolerance for gap size - this is a hard coded
            % precision constant
            decrement = 0.00001;
            
            % make sure the element area isn't too large
            if n_rings*element_area > pi*r2^2
              error('element area is too large for transducer')  
            end


            % initialise inside and outsize matrices
            gap = r2/n_rings; %make this large enough that Radius_diff is always positive
            inner = zeros(n_rings,1); %initialise the array of inner radii values
            outer = zeros(n_rings,1); %initialise the array of outer radii values
            inner(1,1) = r1+gap; %define the radius of the inside of the first ring

            % calculate the position of the first outer ring from this
            outer(1,1) = sqrt((element_area/pi) + (inner(1,1)^2)); 


            % calculate the initial first pass radius based on the guess for G

            % start from the second ring as the first has been calculated
            for j = 2:n_rings
                % calculate the position of the next inner ring
                inner(j,1) = outer(j-1,1) + gap;

                %calculate the position of the next outer ring
                outer(j,1) = sqrt((element_area/pi) + (inner(j,1)^2));
            end

            %define a radius overshoot, ie. how much larger the current transducer
            %configuration is that the required one
            radius_overshoot = outer(n_rings,1) - (r2 - outer_gap);

            %% Repeat the iteration as many times as it takes to get the correct gap size based on tolerances
            while radius_overshoot >= 0;
                %decrease the gap length inbetween the elements
                gap = gap - decrement; 
                for j = 2:n_rings
                    %calculate the new positions of all the rings outside the first ring        
                    inner(j,1) = outer(j-1,1) + gap;
                    outer(j,1) = sqrt((element_area/pi) + (inner(j,1)^2));
                end
    
                %work out the new radius overshoot
                radius_overshoot = outer(n_rings,1) - (r2 - outer_gap); 

                %define the new first ring based on the updated gap size
                inner(1,1) = r1 + gap;
                outer(1,1) = sqrt((element_area/pi) + (inner(1,1)^2));
            end
            
            %create all the rings (no) in a for loop
            for j = 1:n_rings
                %create all of the transducer rings
                obj.create_annular_transducer(width, inner(j,1), outer(j,1), n, r_c);
            end
        end
        
        function [ anulus ] = generate_anulus(obj, n, r1, r2 )
            %GENERATE_ANULUS Creates a square n*n matrix containing an anulus of outer
            %radius r2, inner radius r1 centered within  the square. Elements
            %representing the anulus are given the value of 1 whilst others 0.

            % create the outer circle of the anulus
            center = round(n/2);
            anulus = obj.center_circle_fill(n, r2, 1, center, center);

            % super impose a circle of -1 values to cancel out the interior of the
            % outer circle
            anulus = anulus + obj.center_circle_fill(n, r1, -1, center, center);
        end
        
        function [ matrix ] = center_circle_fill(obj, w, r, value, center_x, center_y)
            %CENTER_CIRCLE_FILL Creates a square matrix containing a circle of values 
            %where the center is specified
            % create a list of all coordinates of a square matrix of width w
            [x, y] = meshgrid(1:w);

            % find all the indicies within the circle and set the values to 1
            matrix = (x-center_x).^2+(y-center_y).^2 <= r^2;

            % set to be the correct value
            matrix = matrix*value;
        end
        
        function set_sim_size(obj, x_coords, y_coords, z_coords)
            %SET_SIM_SIZE Generates coordinates for pressure field
            %calculation. Number of steps in each direction should be set
            %before this (although default values exist)
            
            obj.x_steps = length(x_coords);
            obj.y_steps = length(y_coords);
            obj.z_steps = length(z_coords);
            
            % expand the set of points to a full grid
            [full_x, full_y, full_z] = meshgrid(x_coords, y_coords, z_coords);

            % flatten the points back out
            obj.x_coords = full_x(:);
            obj.y_coords = full_y(:);
            obj.z_coords = full_z(:);
        end
        
        function set_sim_plane_ahead_size(obj, x_coords, z_coords)
            %SET_SIM_PLANE_AHEAD_SIZE Generates coordinates for pressure
            %field calculation of a plane perpendicular to the surface of
            %the transducer (useful for observing focus)
            
            obj.x_steps = length(x_coords);
            obj.z_steps = length(z_coords);
            
            % expand the set of points to a full grid
            [full_x, full_z] = meshgrid(x_coords, z_coords);

            % flatten the points back out
            obj.x_coords = full_x(:);
            obj.y_coords = zeros(size(obj.x_coords));
            obj.z_coords = full_z(:);
        end
        
        function [ count ] = get_transducer_element_count(obj)
            %GET_TRANSDUCER_COUNT Calculates the number of elements
            %currently present in the transducer
            count = size(obj.transducer, 3);
        end
        
        function rayleigh_baffle_piston(obj)
            %RAYLEIGH_BAFFLE_PISTON_AHEAD Estimates the pressure field from a baffled
            %transducer centered on the origin at [0,0].
            
            x_num = length(obj.x_coords);
            y_num = length(obj.y_coords);
            z_num = length(obj.z_coords);

            % validate input
            assert((x_num == y_num) & (x_num == z_num), 'x, y and z vectors are of differnt lengths');

            % preallocate output for speed
            current_pf = zeros(1, x_num);
            
            % calculate the number of transducers
            t_num = obj.get_transducer_element_count();
            
            % give a time estimate for how long the simulation will take
            tic();
            
            for t_idx = 1:t_num
                % take a local copy of the current transducer
                current_transducer = obj.transducer(:,:,t_idx);
                
                % calculate x0 and y0 values
                [w, h] = size(current_transducer);
                x0 = obj.transducer_scale_x(t_idx)*linspace(-w/2, w/2, w);
                y0 = obj.transducer_scale_y(t_idx)*linspace(-h/2, h/2, h);

                % create a mesh to integrate with
                [x0_grid, y0_grid] = meshgrid(x0, y0);
                if obj.r_c(t_idx) > 0
                    z0_grid = obj.r_c(t_idx)-sqrt(abs(obj.r_c(t_idx)^2-x0_grid.^2-y0_grid.^2));
                else
                    z0_grid = zeros(w, h); % todo: check the w and h are the right way round (only tested when equal)
                end
                
                %surf(x0_grid,y0_grid,z0_grid);
                
                % calculate the angular velocity
                w = 2*pi*obj.f(t_idx);

                % calculate the wave number
                k = w/obj.c;
                
                parfor_xcoords = obj.x_coords;
                parfor_ycoords = obj.y_coords;
                parfor_zcoords = obj.z_coords;
                parfor_ztranscoords = obj.r_c(t_idx)-sqrt(abs(obj.r_c(t_idx)^2-parfor_xcoords.^2-parfor_ycoords.^2));
                use_fresnel = obj.use_fresnel_approx;
                
                parfor idx = 1:x_num
                    % calculate the vector of R values for the given x,y,z coord and x0,y0
                    % coordinates
                    if use_fresnel
                        z_dist = parfor_zcoords(idx)-parfor_ztranscoords(idx);
                        z_dist = z_dist*(z_dist>0);
                        R = z_dist + ((parfor_xcoords(idx)-x0_grid).^2 + (parfor_ycoords(idx)-y0_grid).^2)./(2*z_dist);
                    else
                        R = sqrt((parfor_xcoords(idx)-x0_grid).^2+(parfor_ycoords(idx)-y0_grid).^2+(parfor_zcoords(idx)-z0_grid).^2);
                    end
                    
                    % calculate the integrand
                    if use_fresnel
                        fi = current_transducer.*exp(1i*k*R)/z_dist;
                    else
                        fi = current_transducer.*exp(1i*k*R)./R;
                    end

                    % calculate the double integral
                    current_pf(idx) = trapz(y0, trapz(x0, fi, 2));
                end

                % calculate the integral coefficients
                coeffs = -1i*k*obj.p*obj.c*0.5*obj.u0(t_idx)*exp(-1i*w*(obj.t + obj.ps(t_idx)))/pi;

                % multiply by the coefficients and dump the imaginary part
                current_pf = real(current_pf.*coeffs);
                
                % super impose the calculated pressure field or set it to this
                if isempty(obj.pf)
                    obj.pf = current_pf;
                else
                    obj.pf = obj.pf + current_pf;
                end
                % report time estimate
                if t_idx == 1
                    t_one_point = toc;
                    fprintf('Time estimate: %fmins\n', (t_one_point*t_num)/60);
                end
            end
        end
    end
end