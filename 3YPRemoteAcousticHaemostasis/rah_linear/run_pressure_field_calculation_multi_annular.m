%RUN_PRESSURE_FIELD_CALCULATION_ANULUS Demonstrates the rah_linear class
%with an annular transducer

% instantiate rah_linear class
obj = rah_linear;

% set the simulation size 10cm * 10cm with 30 steps in xy directions, 10 in
% z direction
obj.set_sim_size(0.1, 30, 0.1, 30, 4, 5);

% use an annular transducer 5cm * 5cm with inner radius 1cm outer radius
% 5cm and 100 steps of integration in both directions
obj.create_multi_annular_transducer(0.05, 0.1*0.05, 0.5*0.05, 0.025*0.05, 6, 100);

% focus the transducer
obj.focus_annular_transducer(3);

% calculate the power emitted
power = obj.calc_power_emitted()

% run the simulation
obj.rayleigh_baffle_piston();

% plot the transducer
obj.plot_transducer();

% plot the result
figure;
obj.plot_pressure_field();