% Demonstrates pressure field calculation directly ahead of the piston

% density of water (kgm^-2)
p = 1000;

% speed of sound through water (ms^-1)
c = 1482;

% frequency of ultrasound (Hz)
f = 1.5e6; % 1.5MHz

% maximum harmonic surface velocity
u0 = 1;

% time of simulation
t = 1;

% width of the rectangular element (y axis) in meters
width = 0.05;

% height of the rectangular element (x axis) in meters
height = 0.05;

% width (y axis) of the simulation in meters and number of steps to take in
% the simulation
sim_width = 0.1;
sim_width_steps = 50;

% height (x axis) of the simulation in meters and number of steps to take
% in the simulation
sim_height = 0.1;
sim_height_steps = 50;

% maximum depth of field of the simulation (z axis) in meters and number of
% steps to take in the simulation
max_depth = 4;
sim_depth_steps = 10;

% number of steps taken by the integration across the rectangular element
% in the x and y directions
a_steps = 100;
b_steps = 100;

% create a set of points to evaluate pressure field at
x = linspace(-sim_height, sim_height, sim_height_steps);
y = linspace(-sim_width, sim_width, sim_width_steps);
z = linspace(0, max_depth, sim_depth_steps);

% expand the set of points to a full grid
[full_x, full_y, full_z] = meshgrid(x, y, z);

% flatten the points back out
full_x = full_x(:);
full_y = full_y(:);
full_z = full_z(:);

% create a rectangular transducer
transducer = ones(a_steps, b_steps);

% define scaling
scale_x = width/a_steps;
scale_y = height/b_steps;

% run the simulation
pf = rayleigh_baffle_piston(full_x, full_y, full_z, t, transducer, scale_x, scale_y, u0, f, p, c);

% cull the uninteresting results (makes scatter more responsive, comment
% out to keep all data
keep = abs(pf) > 1e5;
pf = pf(keep);
full_x = full_x(keep);
full_y = full_y(keep);
full_z = full_z(keep);

% plot the medium
scatter3(full_x, full_y, full_z, 30, pf,'filled');
%set(gca,'Color',[0 0 0]); % uncomment to set background black

xlabel('x');
ylabel('y');
zlabel('z');
cb = colorbar;
cb.Label.String = 'Pressure';