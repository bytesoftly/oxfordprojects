function [ anulus ] = generate_anulus( n, r1, r2 )
%GENERATE_ANULUS Creates a square n*n matrix containing an anulus of outer
%radius r2, inner radius r1 centered within  the square. Elements
%representing the anulus are given the value of 1 whilst others 0.

% create the outer circle of the anulus
center = round(n/2);
anulus = center_circle_fill(n, r2, 1, center, center);

% super impose a circle of -1 values to cancel out the interior of the
% outer circle
anulus = anulus + center_circle_fill(n, r1, -1, center, center);

end

function [ matrix ] = center_circle_fill(w, r, value, center_x, center_y)
%CENTER_CIRCLE_FILL Creates a square matrix containing a circle of values 
%where the center is specified
    % create a list of all coordinates of a square matrix of width w
    [x, y] = meshgrid(1:w);
    
    % find all the indicies within the circle and set the values to 1
    matrix = (x-center_x).^2+(y-center_y).^2 <= r^2;
   
    % set to be the correct value
    matrix = matrix*value;
end