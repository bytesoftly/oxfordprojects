function [ p ] = rayleigh_baffle_piston_ahead(x, y, z, u, k, p, c)
%RAYLEIGH_BAFFLE_PISTON_AHEAD Estimates the pressure field from a baffled
%rectangular element large enough to contain all [x,y] coordinates. I.e.
%only the region in front of the element is modelled.
%   x = vector of x values
%   y = vector of y values
%   z = vector of depths of field from piston surface to calculate at (m)
%   u = 2d matrix of harmonic surface velocities for every [x,y,z] coordinate on piston (although only valid when z = 0) (?)
%   k = wave number (radsm^-1)
%   p = medium material density (kgm^-3)
%   c = speed of sound (ms^-1)

x_num = length(x);
y_num = length(y);
z_num = length(z);
u_num = length(u);

% validate input
assert((x_num == y_num) & (x_num == z_num), 'x, y and z vectors are of differnt lengths');
assert(u_num == x_num, 'Each coordinate must have a corresponding u value (lengths of u and [x,y,z] don''t match) even though u are only valid at z=0');

% calculate the integral coefficients
coeffs = (-1i*k*p*c*0.5/pi).*u;

% calculate the R values (shortest/perp distance to the element plane).
% Because we are only interested in the region directly in front of the
% element the distance from the element will always be z
R = abs(z);

% calculate the 1/2 width and 1/2 height of the element (which must encapsulate all
% given x,y values given the center as the origin
a = max(max(abs(x)));
b = max(max(abs(y)));

% calculate the integral. Note that the double integral simplifies to 4ab
% when calculating the pressure field in front of the element as it becomes
% independent of x and y.
p = 4*a*b*coeffs.*exp(1i*k.*R)./R;

end

