% Demonstrates pressure field calculation directly ahead of the piston

% density of water (kgm^-2)
p = 1000;

% speed of sound through water (ms^-1)
c = 1482;

% frequency of ultrasound (Hz)
f = 1.5e6; % 1.5MHz

% maximum harmonic surface velocity
u0 = 1;

% time of simulation
t = 1;

% width and height of the square transducer element
width = 0.05;

% number of steps to integrate accross transducer element in x and y
% directions
transducer_steps = 100;

% width (y axis) of the simulation in meters and number of steps to take in
% the simulation
sim_width = 0.1;
sim_width_steps = 30;

% height (x axis) of the simulation in meters and number of steps to take
% in the simulation
sim_height = 0.1;
sim_height_steps = 30;

% maximum depth of field of the simulation (z axis) in meters and number of
% steps to take in the simulation
max_depth = 4;
sim_depth_steps = 5;

% transducer parameters
transducer_element_width = 30;
element_inner_r = 10;

% create a set of points to evaluate pressure field at
x = linspace(-sim_height, sim_height, sim_height_steps);
y = linspace(-sim_width, sim_width, sim_width_steps);
z = linspace(0, max_depth, sim_depth_steps);

% expand the set of points to a full grid
[full_x, full_y, full_z] = meshgrid(x, y, z);

% flatten the points back out
full_x = full_x(:);
full_y = full_y(:);
full_z = full_z(:);

% define scaling
scale_x = width/transducer_steps;
scale_y = scale_x;

% create an annular transducer
transducer = generate_anulus(transducer_steps, element_inner_r, transducer_element_width+element_inner_r);

% run the simulation
pf = rayleigh_baffle_piston(full_x, full_y, full_z, t, transducer, scale_x, scale_y, u0, f, p, c, 0);

% plot the transducer
imshow(transducer);

% cull the uninteresting results (makes scatter more responsive, comment
% out to keep all data
% keep = abs(pf) > 1e5;
% pf = pf(keep);
% full_x = full_x(keep);
% full_y = full_y(keep);
% full_z = full_z(keep);

% plot the medium
figure;
scatter3(full_x, full_y, full_z, 30, pf,'filled');

xlabel('x');
ylabel('y');
zlabel('z');
cb = colorbar;
cb.Label.String = 'Pressure';