function [ pxyz ] = mast_rectangular( x, y, z, k, a, b, Fx, Fy )
%MAST_RECTANGULAR Rectangular element focused in the x- and/or y-axes
% Using expressions from:  T.D. Mast  Fresnel approximations for 
%acoustic fields of rectangularly symmetric sources
%JASA 121(6) 3311 (2007).

%Internal variables for a rectangular element focused in the 
%x- or y-axes

% Based on code by Robin Cleveland

pxyz = zeros(length(x), length(y), length(z));

[~, axmax] = max(size(pxyz));

switch axmax
    case 1
        for ycnt = 1:length(y),
            for zcnt = 1:length(z),
                xt = x;
                yt = y(ycnt);
                r = sqrt(x.^2+y(ycnt)^2+z(zcnt)^2);
                pxyz(:, ycnt, zcnt) = mast_integral(k, a, b, Fx, Fy, xt, yt, r);
            end
        end
    case 2
        for xcnt=1:length(x)
            for zcnt = 1:length(z),
                xt = x(xcnt);
                yt = y;
                r = sqrt(x(xcnt)^2+y.^2+z(zcnt)^2);
                pxyz(xcnt, :, zcnt) = mast_integral(k, a, b, Fx, Fy, xt, yt, r);
            end
        end
    case 3
        for xcnt = 1:length(x),
            for ycnt = 1:length(y),
                xt = x(xcnt);
                yt = y(ycnt);
                r = sqrt(x(xcnt)^2+y(ycnt)^2+z.^2);
                pxyz(xcnt, ycnt, :) = mast_integral(k, a, b, Fx, Fy, xt, yt, r);
            end
        end
end

end

function [p] = mast_integral( k, a, b, Fx, Fy, xt, yt, xi)
%MAST_INTEGRAL
% Rectangular element focused in the x- and/or y-axes
% Using expressions from:  T.D. Mast  Fresnel approximations for 
%acoustic fields of rectangularly symmetric sources
%JASA 121(6) 3311 (2007).

%eps1 = 0;
if Fx>0,
    kxt = k*(1-xi/Fx);
else
    kxt = k;
end

%eta1 = 0;
if Fy>0,
    kyt = k*(1-xi/Fy);
else
    kyt = k;
end

%Variables set by calling
%xt=x+i*eps1*xi/k;
%yt=y+i*eta1*xi/k;
%xt=x;
%yt=y;

%Mast's pressure calculation
p = fcsz((k*xt+kxt*a)./sqrt(pi*kxt.*xi)) - fcsz((k*xt-kxt*a)./sqrt(pi*kxt.*xi));
p = p.*(fcsz((k*yt+kyt*b)./(sqrt(pi*kyt.*xi))) - fcsz((k*yt-kyt*b)./(sqrt(pi*kyt.*xi))));
p = -1i*k*exp(1i*(k*(xi.^2+xi.^2) -k^2*(xt.^2./kxt + yt.^2./kyt) )./(2*xi)).*p./sqrt(4*kxt.*kyt);

end

