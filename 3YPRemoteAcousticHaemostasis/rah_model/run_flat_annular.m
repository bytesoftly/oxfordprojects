% A script to model an annular array of piezoelectric transducers mounted
% on a flat surface (JMC)

% TRANSDUCER PROPERTIES
% number of transducer rings (similar to the Sonic Concepts H-178. 93 Element Array)
ring_count = 4;

% width of transducer rings (mm)
ring_width = 0.5e-3;%5.5;

% width of transducer ring spacing (mm)
ring_spacing = 0.5;

% focal distance in x (0 = no focus)
f_x = 70e-3;

% focal distance in y (0 = no focus);
f_y = f_x; % set to match f_x to approximate a concave transducer element

% frequency of transducer operation (Hz)
freq = 3.5e6;

% MEDIUM PROPERTIES
% speed of sound in medium (m/s)
c0 = 1540;

% use the given library to calculate the pressure field for one transducer
% using the Fresnel approximation to the Rayleigh integral
wave_number = 2*pi*freq/c0;

% y-z plane plot
xvec = 0;
yvec = [-9.9:0.01:10]*1e-3;
zvec = [10.5:0.1:100]*1e-3;

% Do half of one ring (will go in a loop)
p = mast_rectangular(xvec, yvec, zvec, wave_number, 0.5*ring_width, 13e-3/2, f_x, f_y);

imagesc(zvec*1e3, yvec*1e3, squeeze(abs(p)))
xlabel('z (mm)');
ylabel('y (mm)');
