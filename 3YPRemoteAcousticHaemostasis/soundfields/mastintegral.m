%mastintegral
% Rectangular element focused in the x- and/or y-axes
% Using expressions from:  T.D. Mast  Fresnel approximations for 
%acoustic fields of rectangularly symmetric sources
%JASA 121(6) 3311 (2007).


%Internal
xi=r;

eps1=0;
if Fx>0,
    kxt=k*(1-xi/Fx);
else
    kxt=k;
end

eta1=0;
if Fy>0,
    kyt=k*(1-xi/Fy);
else
    kyt=k;
end

%Variables set by calling
%xt=x+i*eps1*xi/k;
%yt=y+i*eta1*xi/k;
%xt=x;
%yt=y;

%Mast's pressure calculation
p=fcsz((k*xt+kxt*a)./sqrt(pi*kxt.*xi)) - fcsz((k*xt-kxt*a)./sqrt(pi*kxt.*xi));
p=p.*(fcsz((k*yt+kyt*b)./(sqrt(pi*kyt.*xi))) - fcsz((k*yt-kyt*b)./(sqrt(pi*kyt.*xi))));
p=-i*k*exp(i*(k*(r.^2+xi.^2) -k^2*(xt.^2./kxt + yt.^2./kyt) )./(2*xi)).*p./sqrt(4*kxt.*kyt);