function pxyz=mastrectangular(x,y,z);
%Rectangular element focused in the x- and/or y-axes
% Using expressions from:  T.D. Mast  Fresnel approximations for 
%acoustic fields of rectangularly symmetric sources
%JASA 121(6) 3311 (2007).

global k a b Fx Fy 

%Internal variables for a rectangular element focused in the 
%x- or y-axes

pxyz=zeros(length(x),length(y),length(z));

[lenmax axmax]=max(size(pxyz));

switch axmax
    case 1
        for ycnt=1:length(y),
            for zcnt=1:length(z),
                xt=x;
                yt=y(ycnt);
                r=sqrt(x.^2+y(ycnt)^2+z(zcnt)^2);
                mastintegral;
                pxyz(:,ycnt,zcnt)=p;
            end
        end
    case 2
        for xcnt=1:length(x)
            for zcnt=1:length(z),
                xt=x(xcnt);
                yt=y;
                r=sqrt(x(xcnt)^2+y.^2+z(zcnt)^2);
                mastintegral;
                pxyz(xcnt,:,zcnt)=p;
            end
        end
    case 3
        for xcnt=1:length(x),
            for ycnt=1:length(y),
                xt=x(xcnt);
                yt=y(ycnt);
                r=sqrt(x(xcnt)^2+y(ycnt)^2+z.^2);
                mastintegral;
                pxyz(xcnt,ycnt,:)=p;
            end
        end
end