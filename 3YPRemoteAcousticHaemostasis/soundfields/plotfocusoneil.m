%Plots axial and radial field of a focused piston using the ONeil solution
% From H.T. O'Neil, Theory of Focusing Radiators, J. Acoust. Soc. Am., Vol
% 21 No 5, pp 516-526 (1949).
%
%Pressure is normalised by P0=rho0*c0*u0 where u0 is the velocity of the
%source.
%
%Needs function files oneilfocal and oneilaxial

%written by Robin Cleveland.
%University of Oxford, November 2012

%Source parameters for diagnostic ultrasound
 f0=3.5e6;  %frequency
 a=15e-3;    %radius of aperture
 roc=70e-3;  %radius of curvature
 
 %Source parameters for therapeutic ultrasound
 f0=1.1e6;    %frequency
 a=32e-3;   %radius of aperture
 roc=63e-3; %radius of curvature

 
c0=1540;  %Speed of sound in medium
 
k=2*pi*f0/c0;       %Wavenumber
xrayl=0.5*k*a*a     %Rayleigh distance
G=xrayl/roc         %Focusing gain

%Calculate axial pressure
zvec=linspace(0.1,2,500)*roc;  %Calculates to twice the focal length in axial direction
pz=oneilaxial(zvec,k,roc,a);

%Calculate radial pressure in the focal plane
rvec=linspace(-5,5,200)*a/G;   %Adjusts radial width depending on gain to give two side-lobes
pr=G*oneilfocal(rvec,k,roc,a);

figure(1);
plot(zvec*1e3,abs(pz));
title(['F=',num2str(roc*1e3),' mm  a=',num2str(a*1e3),' mm  f0=',num2str(f0/1e6),' MHz']);
ylabel('Pressure (p/p0)')
xlabel('Axial distance (mm)')

%return;

figure(2);
plot(rvec*1e3,abs(pr));
title(['F=',num2str(roc*1e3),'mm  a=',num2str(a*1e3),' mm  f0=',num2str(f0/1e6),' MHz']);
ylabel('Pressure (p/p0)')
xlabel('Radial distance (mm)')