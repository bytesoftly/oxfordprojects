%plotfresnelrect
%
%Solution to Fresnel integral for a rectangular uniform source:
%
% Using expressions from:  T.D. Mast  Fresnel approximations for 
%acoustic fields of rectangularly symmetric sources
%JASA 121(6) 3311 (2007).

%written by Robin Cleveland, Boston University, January 2009.

%global properties needed by subroutine
global k a b Fx Fy 

%Transducer properties - single element of a linear array transducer
f0=3.5e6;
a=0.5e-3/2;  %half width in x-axis
b=13e-3/2;   %half width in y-axis
Fx=0;        %Focal distance in x-axis  0=no focus
Fy=70e-3;    %Focal distance in y-axis  0=no focus

%Medium properties
c0=1540;   %Speed of sound

k=2*pi*f0/c0;

%x-axis plot
xvec=[-9.9:0.2:10]*1e-3;
yvec=0;
zvec=70e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(1)
plot(xvec*1e3,abs(p))
xlabel('x (mm)');

%y-axis plot
xvec=0;
yvec=[-9.95:0.1:10]*1e-3;
zvec=70e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(2);
plot(yvec*1e3,abs(p))
xlabel('y (mm)');


%x-axis plot
xvec=0;
yvec=0;
zvec=[10.25:0.5:100]*1e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(3);
plot(zvec*1e3,squeeze(abs(p)))
xlabel('z (mm)');

%x-z plane plot
xvec=[-14.8:0.4:15]*1e-3;
yvec=0;
zvec=[10.5:1:100]*1e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(4);
imagesc(zvec*1e3,xvec*1e3,squeeze(abs(p)))
xlabel('z (mm)');
ylabel('x (mm)');

%y-z plane plot
xvec=0;
yvec=[-9.9:0.2:10]*1e-3;
zvec=[10.5:1:100]*1e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(5);
imagesc(zvec*1e3,yvec*1e3,squeeze(abs(p)))
xlabel('z (mm)');
ylabel('y (mm)');

%x-y plane plot
xvec=[-14.8:0.4:15]*1e-3;
yvec=[-9.9:0.2:10]*1e-3;
zvec=70*1e-3;

p=mastrectangular(xvec,yvec,zvec);

figure(6);
imagesc(yvec*1e3,xvec*1e3,squeeze(abs(p)))
xlabel('y (mm)');
ylabel('x (mm)');