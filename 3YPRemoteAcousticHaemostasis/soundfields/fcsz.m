function csz=fcsz(u);

csz=fcs(real(u))+i*conj(fcs(imag(u)));
