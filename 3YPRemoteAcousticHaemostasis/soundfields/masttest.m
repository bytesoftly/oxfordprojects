
%z=[-9.8:0.4:10]*1e-3;
x=0;
y=[-9.8:0.4:10]*1e-3;
z=[10.5:1:100]*1e-3;

pxyz=zeros(length(x),length(y),length(z));

[lenmax axmax]=max(size(pxyz));

switch axmax
    case 1
        for ycnt=1:length(y),
            for zcnt=1:length(z),
                xt=x;
                yt=y(ynct);
                xi=sqrt(x.^2+y(ycnt)^2+z(zcnt)^2);
                mastintegral;
                pxyz(:,ycnt,zcnt)=p;
            end
        end
    case 2
        for xcnt=1:length(x)
            for zcnt=1:length(z),
                xt=x(xcnt);
                yt=y;
                xi=sqrt(x(xcnt)^2+y.^2+z(zcnt)^2);
                mastintegral;
                pxyz(xcnt,:,zcnt)=p;
            end
        end
    case 3
        for xcnt=1:length(x),
            for ycnt=1:length(y),
                xt=x(xcnt);
                yt=y(ycnt);
                xi=sqrt(x(xcnt)^2+y(ycnt)^2+z.^2);
                mastintegral;
                pxyz(xcnt,ycnt,:)=p;
            end
        end
end

