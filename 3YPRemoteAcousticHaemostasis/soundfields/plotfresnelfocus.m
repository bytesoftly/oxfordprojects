%plotfresnelfocus

%Plots axial and radial field of a focused piston within the Fresnel 
%approximation

%written by Robin Cleveland.
%University of Oxford, November 2012

%Source parameters for diagnostic ultrasound
 f0=3.5e6;  %frequency
 a=15e-3;    %radius of aperture
 roc=70e-3;
 
 %Source parameters for therapeutic ultrasound
 f0=1.1e6;
 a=32e-3;
 roc=63e-3;

%Medium properties
c0=1540;  %Speed of sound in medium

%Some useful parameters
k=2*pi*f0/c0;       %Wavenumber
xrayl=0.5*k*a*a     %Rayleigh distance
G=xrayl/roc         %Focusing gain


%Axial
sigma=linspace(0,2,500);  %Axial distance normalised by Rayleigh distance
pfresnelax=(exp(i*G*(1./sigma - 1))-1)./(sigma-1);

figure(1);
plot(sigma*roc*1e3,abs(pfresnelax))
title(['F=',num2str(roc*1e3),' mm  a=',num2str(a*1e3),' mm  f0=',num2str(f0/1e6),' MHz']);
ylabel('Pressure (p/p0)')
xlabel('Axial distance (mm)')

%Radial
rho=linspace(-6.5/G,6.5/G,100);  %Radial distance normalised by source radius
pfresnelra=2*G*besselj(1,2*G*rho)./(2*G*rho);

figure(2);
plot(rho*a*1e3,abs(pfresnelra))
title(['F=',num2str(roc*1e3),'mm  a=',num2str(a*1e3),' mm  f0=',num2str(f0/1e6),' MHz']);
ylabel('Pressure (p/p0)')
xlabel('Radial distance (mm)')

