%Array Directivity
%Plots directivity of an N element array with element directivity,
%steering and apodisation if desired.

%Written by Robin Cleveland, University of Oxford
%November 2012

%Frequency
f0=2e6;

%Sound speed of medium
c0=1540;
lambda=c0/f0;   %Wavelength

%Number of elements
N=6;

%Distance between elements
d=0.5e-3;

%Element size - must be less than d!
de=0;      %Point source
%de=0.85*d;  %Uncomment for finite sized element.

%Angle to steer beam
%thetamax=0;
thetamax=30*pi/180;   %Uncomment for steering in degrees.

%Apodisation
wn=ones(1,N);   %No apodisation
%Uncomment following line for Hamming apodisation
wn=0.54-0.46*cos(2*pi*[0:N-1]/(N-1));

%Angles over which to calculate directivity
theta=linspace(-90,90,2000);
sintheta=sin(theta*pi/180);

%Directivity of ideal array
psi=pi*d*sin(thetamax)/lambda;   %Offset due to steering
phi=pi*d*sintheta/lambda-psi;
Da=sin(N*phi)./(N*sin(phi));

%Element directivity
if de>0,
    De=sin(pi*de*sintheta/lambda)./(pi*de*sintheta/lambda);
else
    De=1;
end

%Plot directivity - with eleement directivity too if not a point source
figure(1);
if de>0,
    semilogy(theta,abs(De),theta,abs(Da),theta,abs(Da.*De));
    legend('Element','Array','Combination');
else
    semilogy(theta,abs(Da));
end
axis([-90 90 1e-2 1]);
title(['N=' num2str(N) '  d=' num2str(1000*d), ' mm  de=' num2str(1000*de) ' mm']);,

%Calculate apodised array
Daw=0;
for ncnt=1:N,
    Daw=Daw+wn(ncnt)*exp(j*(N-1)*phi).*exp(-j*(ncnt-1)*2*phi);
end
Daw=Daw/sum(wn);

figure(2)
semilogy(theta,abs(Da.*De),theta,abs(Daw.*De));
axis([-90 90 1e-4 1]);
legend('No Apodisation','Apodised Array');
title(['N=' num2str(N) '  d=' num2str(1000*d), ' mm  de=' num2str(1000*de) ' mm']);, 
