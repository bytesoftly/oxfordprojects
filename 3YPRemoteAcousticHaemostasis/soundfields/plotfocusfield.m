%Solution to the acoustic field from a focused piston source
%
%Numerical solution from:
%X Chen, K.Q. Schwarz,  KJ Parker, "Radiation patten of a focused
%transducer: A numerically convergent solution"  JASA 94:2979-2991 1993

%Written by Robin Cleveland, University of Oxford
%December 2012

fname='';      %If this string is empty then data is NOT save
%fname='focustest'; %Data will be saved to a file with this name

%Source parameters for diagnostic ultrasound
 f0=3.5e6;  %frequency
 a=15e-3;    %radius of aperture
 roc=70e-3;   %radius of curvature
 
 %Source parameters for therapeutic ultrasound
 f0=1.1e6;
 a=32e-3;
 roc=63e-3;


%Medium parameters
%c0=1488;    %Sound Speed in water
c0=1540;    %Sound Speed in tissue

%Locations to calculate field

%Example for 1D axial field
%rvec=0.0;
%zvec=[40:0.25:90]*1e-3;

%Example for 1D radial field
%rvec=[0.01:0.01:10]*1e-3;
%zvec=63e-3;

%Example for 2D field
rvec=[0:0.2:10]*1e-3;
zvec=[30:0.5:100]*1e-3;

%Tolerances for summations
Nmax=40;
termtol=1e-4;

%Calculate some useful parameters
k=2*pi*f0/c0;
ka=k*a
alphaHA=asin(a/roc);  %Half angle
G=0.5*k*a*a/roc     %Focusing gain

p=zeros(length(rvec),length(zvec));

%Variables for checking summations
n1=0;n2=0;
nextpc=10;  %At which percentage data is reported
            %if text is uncommented below

for zcnt=1:length(zvec),
    nmax=2;
    for rcnt=1:length(rvec),
        
        r=sqrt(rvec(rcnt)^2+zvec(zcnt)^2);
        theta=atan(rvec(rcnt)/zvec(zcnt));

        Y=(ka*a/r)*(1-(r/roc)*cos(theta));
        Z=ka*sin(theta);
        
        if abs(Y/Z)<1,
            n=0;
            u1p=besselj(1,Z);
            du1p=u1p;
            while (n<2)|((n<Nmax)&(abs(du1p/u1p)>termtol)),
                n=n+1;
                du1p=(-1)^n*(Y/Z)^(2*n)*besselj(2*n+1,Z);
                u1p=u1p+du1p;
            end
            n1=n;
            
            n=0;
            u2p=(Y/Z)*besselj(2,Z);
            du2p=max(1,u2p);
            while (n<2)|((n<Nmax)&(abs(du2p/u2p))>termtol),
                n=n+1;
                du2p=(-1)^n*(Y/Z)^(2*n+1)*besselj(2*n+2,Z);
                u2p=u2p+du2p;
            end
            n2=n;
            
            IYZ=exp(-i*Y/2)*(Y/Z)*(u1p+i*u2p);
            
            if max([n1 n2])>nmax,
                nmax=max([n1 n2]);
                maxerr=[abs(du1p/u1p) abs(du2p/u2p)];
                xmax=rvec(rcnt);
                zmax=zvec(zcnt);
            end
        else
            n=0;
            nu1=(Z/Y)*besselj(1,Z);
            dnu1=nu1;
            while (n<2)|((n<Nmax)&(abs(dnu1/nu1)>termtol)),
                n=n+1;
                dnu1=(-1)^n*(Z/Y)^(2*n+1)*besselj(2*n+1,Z);
                nu1=nu1+dnu1;
            end
            n11=n;
            
            n=0;
            nu0=besselj(0,Z);
            dnu0=nu0;
            while (n<2)|((n<Nmax)&(abs(dnu0/nu0)>termtol)),
                n=n+1;
                dnu0=(-1)^n*(Z/Y)^(2*n)*besselj(2*n,Z);
                nu0=nu0+dnu0;
            end
            n0=n;
            
            IYZ=-i*exp(i*Z*Z/(2*Y))*(1-exp(-i*0.5*(Y+Z^2/Y))*(nu0+i*nu1));
            
            if max([n0 n11])>nmax,
                nmax=max([n0 n11]);
                maxerr=[abs(dnu0/nu0)  abs(dnu1/nu1)];
                rmax=rvec(rcnt);
                zmax=zvec(zcnt);
            end
            
        end

        if abs(Y/Z)>termtol,
            p(rcnt,zcnt)=i*exp(-i*k*r)*IYZ/(1-(r/roc)*cos(theta));
        else
            p(rcnt,zcnt)=G*exp(i*k*roc*(1-1/cos(theta)))*besselj(1,ka*sin(theta))/(0.5*ka*tan(theta));
        end
        
    end

    %Display output during simulation to show progress
    if length(zvec)==1,
        figure(1)
        plot(rvec*1e3,abs(squeeze(p)))
        xlabel('r (mm)');
        
    elseif length(rvec)==1,
        figure(2)
        plot(zvec*1e3,abs(squeeze(p)))
        xlabel('z (mm)');
        
    else
        figure(3)
        imagesc(zvec*1e3,rvec*1e3,abs(p));
        ylabel('r (mm)');
        xlabel('z (mm)');
    end
    
    %Check on how many terms and how accurate solution is
    %if nmax>2,
    %[nmax xmax zmax]
    %maxerr
    %end
    
    % %Print out percent complete every 10%
    %     thispc=round(100*xcnt/length(x));
    %     if thispc>=nextpc,
    %         fprintf(1,[num2str(thispc) '%% ']);
    %         nextpc=nextpc+10;
    %     end
end
% fprintf(1,'\n');

if length(zvec)==1,
    figure(1)
    plot(rvec*1e3,abs(squeeze(p)))
    xlabel('r (mm)');
elseif length(rvec)==1,
    figure(2)
    plot(zvec*1e3,abs(squeeze(p)))
    xlabel('z (mm)');

else
    figure(3)
    imagesc(zvec*1e3,rvec*1e3,abs(p));
    ylabel('r (mm)');
    xlabel('z (mm)');

end

if length(fname)>1,
    save(fname,'f0','c0','a','roc','rvec','zvec','Nmax','termtol','p');
end
