Selection of Matlab files associated with the Biomedical Ultrasonics course
taught at the University of Oxford.  

Scripts written by:
Robin Cleveland
Institute of Biomedical Engineering
Department of Engineering Science
University of Oxford
robin.cleveland@eng.ox.ac.uk

Release 1.0
12 August 2013

The powerpoint file
AcousticFields.pptx
gives a brief description of all the files.  The folder PDF contains some of the 
original papers from which the codes were generated.

Files that can be executed:

UNFOCUSED TRANSDUCERS
plotbaffledpiston.m     on-axis and far field directivity of an unfocused baffled piston
plotpistonfield.m     calculates the field from an unfocused baffled piston
plotpistonimpulse.m   spatial impulse response from an unfocused piston

FOCUSED TRANSDUCERS
plotfocusoneil.m     on-axis and focal plane patterns for a focused piston
plotfocusfield.m     calculates the field from a focused piston
plotarraydirectivity.m  calculates the far-field directivity from an array

FRESNEL APPROXIMATIONS
plotfresnelfocus.m   axial and radial field of a focused piston 
plotfresnelrect.m   field of rectangular source with focusing in either axis

Subroutines that are called by the above files.  
Note you may need to compile the mex code for the Fresnel integrals.  Invoke the Matlab command:
mex fcs.c
to achieve this.  You should only need to do this once.

mastintegral.mmastrectangular.mmasttest.moneilaxial.moneilfocal.m
circimpulse.mdphi.m
fcsz.m
fcs.m